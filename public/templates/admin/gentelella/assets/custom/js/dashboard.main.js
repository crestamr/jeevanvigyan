/**
 * Created by puncoz on 1/12/17.
 */

$(function () {

    // Data Table
    $(".data-table").DataTable();

    // Chosen
    $(".chosen-select").chosen();

    // Bootstrap Datepicker
    $(".datepicker").datepicker({
        autoclose: true,
        clearBtn: true,
        todayBtn: "linked",
        format: "yyyy-mm-dd",
        todayHighlight: true
    });
    $(".daterange").datepicker({
        inputs: $('.datepicker')
    });
    
    // UCFirst
    $.fn.ucfirst = function (word) {
        return word.substr(0, 1).toUpperCase() + word.substr(1);
    }

    // pNotify options
    $.fn.notification = function(type, msg) {
        new PNotify({
            title: $.fn.ucfirst(type),
            text: msg,
            type: type,
            styling: 'bootstrap3'
        })
    };
    setTimeout(function() {
        if (success_msg != "") $.fn.notification('success', success_msg);
        if (error_msg != "") $.fn.notification('error', error_msg);
        if (info_msg != "") $.fn.notification('info', info_msg);
        if (warning_msg != "") $.fn.notification('warning', warning_msg);
    }, 500);

    // Load Form Bootbox
    $(document).off("click", ".loadForm").on("click", ".loadForm", function(event) {
        event.preventDefault();

        var obj = $(this),
            link = obj.attr('href'),
            successBtn = obj.data('successbtn'),
            title = obj.attr('title'),
            addClass = obj.data('addclass');

        $.fn.loadForm($(this), link, successBtn, title, addClass);
    });

    // Confirm Dialog
    $(document).off("click", ".confirmDiag").on("click", ".confirmDiag", function(event) {
        event.preventDefault();
        var obj = $(this),
            msg = obj.data('msg').split("?"),
            link = obj.attr('href'),
            confirmBtn = obj.data('confirmbtn');

        swal({
                title: msg.shift()+"?",
                text: msg.toString(),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: confirmBtn
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.replace(link);
                }
            });
    });

});