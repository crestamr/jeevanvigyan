<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 2:48 PM
 */
$viewData = [];
?>
<?php theme('partials', 'htmlhead', $viewData) ?>
<body class="nav-md">
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<noscript><p class="alert alert-danger">You must enable <strong>javascript</strong> for better experiences.</p></noscript>

<div class="container body">
    <div class="main_container">

        <!-- Side Bar -->
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php theme('partials', 'sidebar', $viewData) ?>
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <?php theme('partials', 'topnav', $viewData) ?>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <!-- Title -->
                <div class="page-title">
                    <div class="title_left">
                        <h3><?=$pageDetail->title?></h3>
                    </div>

                    <!-- BreadCrumb -->
                    <div class="title_right">
                        <div class="pull-right">
                            <?=breadcrumbOutput()?>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row animated slideInRight">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?=$main_body_content?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <?php theme('partials', 'footer', $viewData) ?>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!--
// SCRIPTS
-->
<?php theme('partials/scripts', 'scripts', $viewData) ?>

</body>
</html>

