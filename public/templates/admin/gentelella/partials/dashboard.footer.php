<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 3:31 PM
 */
?>
<div class="pull-right">
    <?=$this->lang->line('software_name')?> - Admin Panel | Powered by <a href="https://mylifemark.com">MyLifeMark</a>
</div>
<div class="clearfix"></div>
