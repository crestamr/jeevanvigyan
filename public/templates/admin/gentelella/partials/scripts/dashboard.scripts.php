<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 3:02 PM
 */
theme('partials/global', 'scripts', [], false, null, null) ?>

<!-- Vendor scripts -->
<script src="<?=assets('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="<?=assets('bower_components/jquery-validation/dist/jquery.validate.min.js')?>"></script>
<script src="<?=assets('bower_components/fastclick/lib/fastclick.js')?>"></script>
<script src="<?=assets('bower_components/nprogress/nprogress.js')?>"></script>
<script src="<?=assets('bower_components/iCheck/icheck.min.js')?>"></script>
<script src="<?=assets('lib/datatable/datatables.min.js', 'admin')?>"></script>
<script src="<?=assets('bower_components/bootbox.js/bootbox.js')?>"></script>
<script src="<?=assets('bower_components/sweetalert/dist/sweetalert.min.js')?>"></script>
<script src="<?=assets('bower_components/chosen/chosen.jquery.js')?>"></script>
<script src="<?=assets('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>

<script src="<?=assets('bower_components/pnotify/dist/pnotify.js')?>"></script>
<script src="<?=assets('bower_components/pnotify/dist/pnotify.buttons.js')?>"></script>

<script src="<?=assets('bower_components/dropzone/dist/min/dropzone.min.js')?>"></script>

<!-- App scripts -->
<script type="text/javascript" src="<?=assets('theme/js/theme.min.js', 'admin')?>"></script>
<script type="text/javascript" src="<?=assets('custom/js/dashboard.main.js', 'admin')?>"></script>

<script src="<?=assets('custom/js/helper/loadForm.js', 'admin')?>"></script>
<script src="<?=assets('custom/js/helper/formValidate.js', 'admin')?>"></script>

<!-- Page-Specific Scripts -->
<?=pageSpecificScripts()?>