<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/25/17
 * Time: 4:59 PM
 */
?>
<script src="<?=assets('lib/fckeditor/fckeditor.js', 'admin')?>"></script>
<script type="text/javascript">
    var base = base_url;
    var oFCKeditor = new FCKeditor('content');
    oFCKeditor.BasePath = "<?=assets('lib/fckeditor', 'admin')?>/";
    oFCKeditor.Height="400px";
    oFCKeditor.Width="100%";
    oFCKeditor.ToolbarSet="Default";
    oFCKeditor.ReplaceTextarea() ;
</script>
<script type="text/javascript">
    var base = base_url;
    var oFCKeditor = new FCKeditor('content_np');
    oFCKeditor.BasePath = "<?=assets('lib/fckeditor', 'admin')?>/";
    oFCKeditor.Height="400px";
    oFCKeditor.Width="100%";
    oFCKeditor.ToolbarSet="Default";
    oFCKeditor.ReplaceTextarea() ;
</script>
