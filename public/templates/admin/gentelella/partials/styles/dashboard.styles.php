<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 2:56 PM
 */
?>
<!--
// STYLESHEETS
-->
<!-- Vendor styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/font-awesome/css/font-awesome.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/animate.css/animate.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/nprogress/nprogress.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/iCheck/skins/flat/green.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('lib/datatable/datatables.min.css', 'admin')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/sweetalert/dist/sweetalert.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/chosen/chosen.css')?>" />

<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/pnotify/dist/pnotify.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/pnotify/dist/pnotify.buttons.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/dropzone/dist/min/dropzone.min.css')?>" />

<!-- App styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('theme/css/theme.min.css', 'admin')?>">

<!-- Custom Styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('custom/css/dashboard.styles.css', 'admin') ?>">

<!--
// IE CORRECTION
-->
<!--[if lt IE 9]>
<script src="<?=assets('bower_components/html5shiv/dist/html5shiv.min.js') ?>"></script>
<script src="<?=assets('bower_components/respond/dest/respond.min.js') ?>"></script>
<![endif]-->
