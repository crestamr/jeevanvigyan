/**
 * Created by puncoz on 1/21/17.
 */
$(function () {

    // Youtube Video Player in meditation
    var $videoLinkFirstImage = $('.videolink img').first();
    var latestVideo = $videoLinkFirstImage.attr("data-videoId");
    $videoLinkFirstImage.addClass('imagepie thumbnail');
    $('.video-container iframe').attr("src", "https://www.youtube.com/embed/" + latestVideo + "?rel=0&amp;showinfo=0").fadeIn(300);

    $('.videoicon').on('click', function () {
        $('.videoicon.thumbnail').removeClass('imagepie thumbnail');
        $(this).addClass('imagepie thumbnail');
        var videoId = $(this).attr('data-videoId');
        $('.video-container iframe').attr("src", "https://www.youtube.com/embed/" + videoId + "?rel=0&amp;showinfo=0").fadeIn(300);
    });

    $('.panel' ).on('click',function () {

        var $more_less_class_attr = $(this).find('.more-less');
        if ( $more_less_class_attr.attr('class') == "more-less glyphicon glyphicon-chevron-up") {
            $(this).siblings().find('.more-less').attr('class', 'more-less glyphicon glyphicon-chevron-down');
            $more_less_class_attr.attr('class', 'more-less glyphicon glyphicon-chevron-down')
        } else {
            $(this).siblings().find('.more-less').attr('class', 'more-less glyphicon glyphicon-chevron-down');
            $more_less_class_attr.attr('class', 'more-less glyphicon glyphicon-chevron-up')
        }
    })

    // // WaterWheel Carousel
    // var carousel = $('.program-carousel').waterwheelCarousel({
    //     flankingItems: 3
    // });
    // $('#prev').bind('click', function () {
    //     carousel.prev();
    //     return false;
    // });
    // $('#next').bind('click', function () {
    //     carousel.next();
    //     return false;
    // });

    $mq = $('.marquee').marquee({
        //duration in milliseconds of the marquee
        duration: 15000,
        //gap in pixels between the tickers
        gap: 50,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'up',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true
    });

    $('.toggleMarquee').hover(function(){
        $mq.marquee('toggle');
    });


});