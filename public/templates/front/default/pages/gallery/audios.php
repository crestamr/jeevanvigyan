<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 11:03 AM
 */
?>

<div class="c-heading">
    <h2><?=($lang === 'np' ? $album->name_np: $album->name)?>
        <span><?=($lang === 'np' ? $album->description_np: $album->description)?></span>
    </h2>
</div>

<div class="c-content">

    <?php foreach ($audios as $audio): ?>
        <div class="col-sm-4 col-md-3 col-lg-2">
            <div class="thumbnail imagepie" >
                <img src="<?=assets('custom/img/audio-thumb-placeholder.jpg', 'admin')?>" alt="meditation.png" class="image_img">
                <div class="caption">
                    <p>
                        <?=($lang === 'np' ? $audio->caption_np: $audio->caption)?>
                    </p>
                    <p><a href="<?=base_url(config_item('file_upload_dir_user')['gallery_audio'].$audio->file)?>" target="_blank" class="btn btn-xs btn-preview"><span class="glyphicon glyphicon-eye-open"></span> Listen</a>
                    </p>
                </div>
            </div>
        </div>
    <?php endforeach;
    if (count($audios) == 0): ?>
        <div class="alert alert-info" role="alert">No audio uploaded in this album yet.</div>
    <?php endif ?>

</div>
