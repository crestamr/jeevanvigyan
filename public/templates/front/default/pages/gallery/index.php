<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 10:00 AM
 */
?>

<div class="c-heading">
    <h2>Gallery</h2>
</div>

<div class="c-content">

    <?php foreach ($albums as $album): ?>
        <a href="<?=front_url(sprintf("gallery/%s/%s", strtolower($albumType[$album->type]), url_encrypt($album->id)))?>">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="thumbnail applepie" >
                    <img src="<?=base_url(config_item('file_upload_dir_user')['gallery_album'].$album->image)?>" alt="meditation.png" class="thumb_img" style="">
                    <div class="caption">
                        <h3 title="<?=($lang === 'np' ? $album->description_np: $album->description)?>"><?=($lang === 'np' ? $album->name_np: $album->name)?></h3>
                        <p><?=date('j F, Y', strtotime($album->created_at))?></p>
                        <p><span class="btn btn-primary"><?=sprintf("%s Album", $albumType[$album->type])?></span></p>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach;
    if (count($albums) == 0): ?>
        <div class="alert alert-info" role="alert">No albums uploaded in gallery yet.</div>
    <?php endif ?>

</div>
