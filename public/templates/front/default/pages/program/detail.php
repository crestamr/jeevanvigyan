<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 12:27
 */
?>

<div class="c-program-detail">
    <div class="col-sm-8" style="background: white; padding-bottom: 10px">
        <h2 style="text-align: center"><?= ($lang == 'np' ? $program->title_np : $program->title) ?></h2>
        <hr>
        <div class="col-sm-3">
            <div class="row">
                <img height="200px" width="100%"
                     src="<?= base_url(Utility::getUploadDir('program').$program->image) ?>?>"
                     alt="<?= $program->slug ?>">
            </div>
        </div>
        <div class="col-sm-9">
            <p style="text-align: left;"><?= ($lang == 'np' ? $program->description_np : $program->description) ?></p>
        </div>
    </div>

    <div class="col-sm-4">

        <?php if ( $program->video ) { ?>
            <div class="video-container">
                <div id="player"></div>
            </div>
        <?php } else {
            echo "<h4 style='text-align: center'>Sorry!! No Video Available</h4>";
        } ?>
    </div>

</div>

<script>
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '300',
            width: '420',
            videoId: '<?= $program->video; ?>',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
</script>
