<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 12:27
 */
?>
<div class="c-heading">
    <h2><?= ($lang === 'np' ? 'कार्यक्रमहरु' : ' Program List') ?></h2>
</div>

<div class="c-content row">
    <?php foreach ($programmes as $programme) { ?>
        <div class="col-sm-3">
            <br>
            <div class="c-program__box" style="height: 300px; width:100%;border:1px solid #570E0F; margin:auto; background:white;">
                <div class=" c-program--logo">
                    <img 
                         height="160px"
                         width="100%"
                         
                         src="<?= base_url(Utility::getUploadDir('program') . $programme->image) ?>"
                         alt="<?= ($lang == 'np' ? $programme->title_np : $programme->title) ?>">
                </div>
                <h2><?= ($lang == 'np' ? $programme->title_np : $programme->title) ?></h2>
                <!-- <p><?= word_limiter($lang == 'np' ? $programme->description_np : $programme->description,3) ?></p> -->
                <a href="<?= front_url(sprintf("program/%s", $programme->slug)) ?>"
                   class="btn btn--readmore"><?= $lang === 'np' ? 'अझ बढी
 ' : 'Read more' ?></a>
            </div>
        </div>
    <?php }
    if (!$programmes) {
        ?>

        No Programmes for now

        <?php
    }
    ?>

</div>
<div class="pagination-links pull-right row">
    <?= isset($programPagination) ? $programPagination : '' ?>
</div>

