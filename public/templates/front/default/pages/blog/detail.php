<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 10:23 AM
 */
?>

<header class="row article">
    <main class="col-sm-8 news" style="background:white;">

        <header>
            <h2><?=($lang === 'np' ? $blog->title_np : $blog->title)?></h2>
            <time><?=date('Y-m-d', strtotime($blog->pub_date))?></time>
        </header>

        <div class="c-content" style="padding-right:10px;padding-left:10px;">
            <div>
                <img style="height:auto;width:725px;" src="<?=base_url(sprintf("%s/%s", Utility::getUploadDir('pages'), $blog->image))?>" class="thumbnail">
            </div>
            <div>
                <?=($lang === 'np' ? $blog->content_np : $blog->content)?>
            </div>
        </div>

    </main>

    <?php theme('partials', 'sidebar', []) ?>
</header>
