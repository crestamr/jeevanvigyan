<div class="c-heading">
    <h2><?= ($lang === 'np' ? 'हाम्रो बारेमा' : 'About Us') ?></h2>
</div>
<div class="row" style="background:white; padding-left: 10px;">
    <div style="padding-top: 10px; float: left; padding-right: 30px ;">
        <div class="thumbnail">
            <a href="<?= base_url(config_item('file_upload_dir_user')['about'] . $about->image) ?>"
               data-lightbox="image-gallery">
                <img src="<?= base_url(config_item('file_upload_dir_user')['about'] . $about->image) ?>"
                     width="400px">
            </a>
        </div>
    </div>
    <div style="margin-top: 10px; ">
        <p style="text-align: left;"><?= ($lang == 'np' ? $about->about_intro_np : $about->about_intro) ?></p>
    </div>

</div>



