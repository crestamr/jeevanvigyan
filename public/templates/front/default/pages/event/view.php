<div class="c-heading">
    <h2><?= ($lang === 'np' ? 'घटना विवरण' : 'Events Details') ?></h2>
</div>
<div>
    <div class="row" style="background:white;">
        <div class="col-md-4" style="padding-top: 10px;">
            <div class="thumbnail">
                <a href="<?= base_url(config_item('file_upload_dir_user')['events'] . $event->cover_pics) ?>"
                   data-lightbox="image-gallery">
                    <img src="<?= base_url(config_item('file_upload_dir_user')['events'] . $event->cover_pics) ?>">
                </a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="c-heading">
                <div class="row" style="margin-top: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <strong><?= ($lang === 'np' ? 'सुरु हुने मिति:' : 'Start Date:') ?></strong>
                            <div><code><?= date("M d, l", strtotime($event->date_start)); ?></code></div>
                        </div>
                        <div class="col-md-6">
                            <h2 style="margin-bottom:0px;color: black;"><?= ($lang === 'np' ? $event->title_np : $event->title) ?></h2>
                        </div>
                        <div class="col-md-3">
                            <strong><?= ($lang === 'np' ? 'सकिने मिति:' : 'End Date:') ?></strong>
                            <div><code>
                                    <?= date("M d, l", strtotime($event->date_end)); ?>
                                </code></div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <p style="text-align: left;"><?= ($lang === 'np' ? $event->description_np : $event->description) ?></p>
        </div>
    </div>
</div>



