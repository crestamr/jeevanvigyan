<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 03/08/2017
 * Time: 17:04
 */
echo "<h1>Audio Album: " . $album->name . '</h1><br>';

?>

<div class="c-content">
        <?php foreach ( $audios as $audio ): ?>
            
            <div class="col-md-4" style="height: 10rem;">
                <audio controls>
                    <source src="<?=base_url(config_item('file_upload_dir_user')['gallery_audio'].$audio->file)?>">
                </audio>
                <p style="font-weight: bolder">
                   <?=($lang === 'np' ? $audio->title_np: $audio->title)?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
    <?php
    if ( count($audios) == 0 ): ?>
        <div class="alert alert-info" role="alert">No audio uploaded in this album yet.</div>
    <?php endif ?>
