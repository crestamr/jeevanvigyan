<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 03/08/2017
 * Time: 17:04
 */
?>


<div class="c-heading">
    <h2>Album: Audios</h2>
</div>

<div class="c-content">

    <?php foreach ($albums as $album): ?>
        <a href="<?=front_url(sprintf("audios/album/%s", $album->slug))?>">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="thumbnail applepie" >
                    <img src="<?=base_url(config_item('file_upload_dir_user')['gallery_album'].$album->image)?>" alt="" class="thumb_img" style="">
                    <div class="caption">
                        <h4 style="font-weight: bolder; color: black;" title="<?=($lang === 'np' ? $album->description_np: $album->description)?>"><?=($lang === 'np' ? $album->name_np: $album->name)?></h4>
                        <p><?=date('j F, Y', strtotime($album->created_at))?></p>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach;
    if (count($albums) == 0): ?>
        <div class="alert alert-info" role="alert">No Image albums uploaded in gallery yet.</div>
    <?php endif ?>

</div>


