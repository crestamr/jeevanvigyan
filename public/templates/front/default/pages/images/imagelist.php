<div class="c-heading">
    <h2 style="color: black;font-weight: bold; text-align: left;">Image Album: </h2>
    <h2 style="color:black;;text-align:left;font-weight: 100"><?= $album->name  ?></h2>
</div>
<?php foreach ($images as $image) { ?>

    <div class="col-sm-3">
        <a href="<?= base_url(config_item('file_upload_dir_user')['gallery_images'] . $image->file) ?>"
           data-lightbox="image-gallery">
            <img src="<?= base_url(config_item('file_upload_dir_user')['gallery_images'] . $image->file) ?>"
                 class="thumbnail" height="200px" width="300px">
        </a>
    </div>
<?php } ?>


<div class="pagination-links pull-right row">
    <?=isset($imagePagination)?$imagePagination:''?>
</div>

