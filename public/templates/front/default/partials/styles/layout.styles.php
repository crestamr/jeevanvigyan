<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 1:03 AM
 */
?>
<!--
// STYLESHEETS
-->
<!-- Vendor styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/font-awesome/css/font-awesome.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/animate.css/animate.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/lightbox2/dist/css/lightbox.css')?>" />


<!-- App Styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('theme/css/theme.css', 'front') ?>">
<link rel="stylesheet" type="text/css" href="<?=assets('custom/css/layout.main.css', 'front') ?>">
<link rel="stylesheet" type="text/css" href="<?=assets('custom/css/responsive.main.css', 'front') ?>">

<!--
// IE CORRECTION
-->
<!--[if lt IE 9]>
<script src="<?=assets('bower_components/html5shiv/dist/html5shiv.min.js') ?>"></script>
<script src="<?=assets('bower_components/respond/dest/respond.min.js') ?>"></script>
<![endif]-->
