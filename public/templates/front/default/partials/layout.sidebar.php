<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 8:55 AM
 */
?>

<aside class="col-sm-4">
    <strong><h2><?=($lang === 'np' ? 'कार्यक्रमहरु' : 'Programmes')?></h2></strong>
    <hr>
    <div class="">
        <div class="list-group">
            <?php
            $allProgram = [];
            $allProgram = $this->program->getAll(true, 8);
            foreach ($allProgram as $programme) { ?>
                <a href="<?= front_url(sprintf("program/%s", $programme->slug)) ?>" class="list-group-item">
                    <?= ($lang == 'np' ? $programme->title_np : $programme->title) ?>
                </a>
            <?php }
            if (!$allProgram) {
                ?>

                No Programmes

                <?php
            }
            ?>

        </div>
    </div>
    <strong><h2><?=($lang === 'np' ? 'भनाइहरु' : 'Quotations')?></h2></strong><hr>
    <div class="" style="background: white">
        <hr>
        <div class="list-group">
            <?php
            $allQuotations = [];
            $allQuotations = $this->quotations->getAll(true, 8);
            foreach ($allQuotations as $quotation)  { if($quotation->language == $lang){ ?>
                <div style="text-align: center">
                    <q><?= $quotation->quotation ?></q>
                </div>
                <hr>
            <?php }}
            if (!$allQuotations) {
                ?>

                No Quotations

                <?php
            }
            ?>

        </div>
    </div>

</aside>
