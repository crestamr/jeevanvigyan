<div class="row">

    <!-- Top Row -->
    <div class="col-sm-12">
        <div class="c-top" style="background: #f0f0f0;">
            <div class="container">
                <div class="row">
                    <div class="c-top__wrapper clearfix">

                        <!-- Contacts -->
                        <div class="col-sm-5">
                            <div class="mobile_contact_language">
                                <div class="mobile_language pull-right">
                                    <a href="<?= front_url('home/lang/en?page=' . $this->uri->uri_string()) ?>"><img
                                                src="<?= assets('images/uk.png', 'front') ?>"></a>
                                    <a href="<?= front_url('home/lang/np?page=' . $this->uri->uri_string()) ?>"><img
                                                src="<?= assets('images/nepal.png', 'front') ?>"></a>
                                </div>
                                <div class="mobile_contact">
                                    <ul class="c-contact">
                                        <li class="c-contact__mail">
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                            <?= safe_mailto('info@jeevanvigyan.com', 'info@jeevanvigyan.com', ['style' => 'color: #333;padding-left: 4px;']) ?>
                                        </li>
                                        <li class="c-contact__phone">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <a href="tel:97714472830"
                                               style="color: #333; padding-left: 4px;">+977-1-4472830</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /Contacts -->

                        <!-- Social -->
                        <div class="col-sm-4">
                            <div class="mobile_social_login">
                                <a href="#" class="btn btn-primary login_button pull-right">Login</a>
                                <ul class="top__social">
                                    <?php
                                    $socialMenus = [];
                                    $socialMenus = $this->social->getAll(true);
                                    foreach ($socialMenus as $socialMenu):
                                        ?>
                                        <li>
                                            <a href="<?= $socialMenu->link ?>" class="<?= $socialMenu->title ?>"
                                               rel="noopener noreferrer" target="_blank"><img
                                                        src="<?= base_url(Utility::getUploadDir('menu_social') . $socialMenu->icon) ?>"
                                                        alt="<?= $socialMenu->name ?>"></a>

                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- /Social -->

                        <!-- Top Menu -->
                        <div class="col-sm-3 text-right">
                            <ul class="c-lang__language">
                                <li><a href="#" class="btn btn-primary login-btn">Login</a></li>
                                <li><a href="<?= front_url('home/lang/en?page=' . $this->uri->uri_string()) ?>"><img
                                                src="<?= assets('images/uk.png', 'front') ?>"></a></li>
                                <li><a href="<?= front_url('home/lang/np?page=' . $this->uri->uri_string()) ?>"><img
                                                src="<?= assets('images/nepal.png', 'front') ?>"></a></li>
                            </ul>
                        </div>
                        <!-- /Top Menu -->
                    </div>
                </div>
                <div class="row" style="text-align: center; font-weight: bold">
                    <?=($lang == 'np' ? 'यो वेबसाइट निर्माणाधिन भएको हुनाले, भएको  त्रुटीहरु प्रति क्षमाप्रार्थी छौं' : 'We are upgrading our Website for your better experience. Sorry, for the inconveniences.') ?>

                </div>
            </div>
        </div>
    </div>
    <!-- /Top Row -->

    <!-- Navigation Menu-->
    <div class="col-sm-12">
        <div class="c-nav">
            <div class="container">
                <div class="row">

                    <!-- Logo -->
                    <!--                    <div class="col-sm-3 col-md-3">-->
                    <!--                        <div class="c-nav__logo">-->
                    <!--                            <a href="--><? //= front_url() ?><!--"><img src="-->
                    <? //= assets('images/logo.png', 'front') ?><!--"-->
                    <!--                                                              alt="Jeevan Vigyan"></a>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!-- /Logo -->

                    <!-- Menu -->
                    <div class="">
                        <nav class="navbar">
                            <div class="navbar-header ">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar--menu" aria-expanded="false">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="c-nav__logo">
                                    <a href="<?= front_url() ?>"><img src="<?= assets('images/logo.png', 'front') ?>"
                                                                      alt="Jeevan Vigyan"></a>
                                </div>
                            </div>

                            <div class="collapse navbar-collapse" id="navbar--menu">
                                <!-- Menu starts -->
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="<?= activeNav('home') ?>"><a href="<?= front_url() ?>"><?=($lang == 'np' ? 'गृह पृष्ठ' : 'Home') ?> </a></li>
                                    <li class="<?= activeNav('program') ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false"><?=($lang == 'np' ? 'कार्यक्रमहरू' : 'Program') ?><span
                                                    class="caret"></span></a>
                                        <ul class="dropdown-menu">

                                            <?php
                                            $allProgram = [];
                                            $allProgram = $this->program->getAll(true, 4);
                                            foreach ($allProgram as $programme) { ?>
                                                <li class="<?= activeNav(['program', $programme->title]) ?>">
                                                    <a href="<?= front_url(sprintf("program/%s", $programme->slug)) ?>">
                                                        <?= ($lang == 'np' ? $programme->title_np : $programme->title) ?>
                                                    </a>
                                                </li>
                                            <?php } ?>

                                            <li><a style="text-align: center"
                                                   href="<?= front_url('program') ?>"><strong><?=($lang == 'np' ? 'सबै हेर्नुहोस' : 'See All') ?></strong></a>
                                            </li><?php
                                            if (!$allProgram) {
                                                ?>
                                                <?= ($lang == 'np' ? 'कुनै कार्यक्रमहरू छैनन्' : 'No Programmes') ?>

                                                <?php
                                            }
                                            ?>

                                        </ul>
                                    </li>
                                    <!--                                    <li>-->
                                    <!--                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"-->
                                    <!--                                           aria-haspopup="true" aria-expanded="false">Activities <span-->
                                    <!--                                                    class="caret"></span></a>-->
                                    <!--                                        <ul class="dropdown-menu">-->
                                    <!--                                            <li><a href="#">dropdown</a></li>-->
                                    <!--                                        </ul>-->
                                    <!--                                    </li>-->
                                    <li class="<?= activeNav('images') ?> <?= activeNav('audios') ?> <?= activeNav('videos') ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true"
                                           aria-expanded="false"><?= ($lang === 'np' ? 'प्रकाशनहरू' : 'Publications') ?>
                                            <span
                                                    class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li class="<?= activeNav('images') ?>"><a
                                                        href="<?= front_url('images') ?>"><?= ($lang === 'np' ? 'तस्बिरहरु' : 'Images') ?></a>
                                            </li>
                                            <li class="<?= activeNav('videos') ?>"><a
                                                        href="<?= front_url('videos') ?>"><?= ($lang === 'np' ? 'भिडियोहरू' : 'Videos') ?></a>
                                            </li>
                                            <li class="<?= activeNav('audios') ?>"><a
                                                        href="<?= front_url('audios') ?>"><?= ($lang === 'np' ? 'अडियो' : 'Audios') ?></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="<?= activeNav('event') ?>"><a
                                                href="<?= front_url("event") ?>"><?= ($lang === 'np' ? 'घटनाहरू' : 'Events') ?></a>
                                    </li>
                                    <li class="<?= activeNav('news') ?>"><a
                                                href="<?= front_url("news") ?>"><?= ($lang === 'np' ? 'समाचार' : 'News') ?></a>
                                    </li>
                                    <li class="<?= activeNav('blog') ?>"><a
                                                href="<?= front_url('blog') ?>"><?= ($lang === 'np' ? 'ब्लग' : 'Blog') ?></a>
                                    </li>
                                    <li class="<?= activeNav('meditation') ?>"><a
                                                href="<?= front_url("meditation") ?>"><?= ($lang === 'np' ? 'ध्यान' : 'Meditation') ?></a>
                                    </li>
                                    <li class="<?= activeNav('about') ?>"><a
                                                href="<?= front_url("about") ?>"><?= ($lang === 'np' ? ' हाम्रो बारेमा' : 'About') ?></a>
                                    </li>
                                    <li class="<?= activeNav('contact') ?>"><a
                                                href="<?= front_url("contact") ?>"><?= ($lang === 'np' ? 'सम्पर्क गर्नुहोस्' : 'Contact Us') ?></a>
                                    </li>
                                    <li class="<?= activeNav('FAQ') ?>"><a
                                                href="<?= front_url("faq") ?>"><?= ($lang === 'np' ? 'सामान्यतः सोधिने प्रश्नहरू ' : 'FAQ') ?></a>
                                    </li>
                                </ul>
                                <!-- Menu Ends -->

                            </div>
                        </nav>
                    </div>
                    <!-- /Menu-->
                </div>
            </div>
        </div>
    </div>
    <!-- /Navigation Menu-->

</div>
