<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 12:53 AM
 */
$viewData = [];
?>
<?php theme('partials', 'htmlhead', $viewData) ?>
<body>

<!-- Header -->
<header class="l-header">
    <?php theme('partials', 'header', $viewData) ?>
</header>

<!-- Home Slider -->
<div class="l-banner">
    <?php theme('partials', 'slider', $viewData, false, '.php', 'home') ?>
</div>

<main class="l-main">

    <!-- Introduction -->
    <section class="c-intro" style="margin-bottom: 70px;">
        <div class="container">
            <div class="c-heading">
                <h2><?= ($lang === 'np' ? 'परिचय' : 'Introduction') ?></h2>
            </div>
            <div class="c-content">
                <p><?= ($lang == 'np' ? $about->home_intro_np : $about->home_intro) ?></p>
            </div>
        </div>
    </section>
    <!-- /Introduction -->

    <!-- Updates -->
    <section class="c-update" id="latest-update">
        <div class="container">
            <div class="">
                <div class="c-heading">
                    <h2><?= ($lang === 'np' ? 'नवीनतम घटना' : 'Upcoming Event') ?></h2>
                    <span><?= $lang === 'np' ? 'जीवन बिज्ञान केन्द्रको यो खण्डमा प्राय: अद्यावधिक गर्दै' : 'Updating frequently in this section of jeevan vigyan kendra' ?></span>
                </div>
                <div class="c-content">
                    <div class="row">

                        <div class="col-sm-12 col-md-6">
                            <div class="c-update__module">
                                <div class="module__img">
                                    <img src="<?= base_url(config_item('file_upload_dir_user')['events'].$event->cover_pics) ?>"
                                         alt="Leadership Module" class="img-responsive latest">
                                </div>
                                <div class="module__content text-right">
                                    <div class="event_header">
                                        <div class="col-md-3" style="padding: 0;margin: 0;">
                                            <strong><?= ($lang === 'np' ? 'सुरु हुने मिति:' : 'Start Date:') ?></strong>
                                            <div><code><?= date("M d, l", strtotime($event->date_start)); ?></code>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;">
                                            <h2><?= ($lang === 'np' ? $event->title_np : $event->title) ?></h2>
                                        </div>
                                        <div class="col-md-3" style="padding: 0;margin: 0; margin-top:3px;">

                                            <strong><?= ($lang === 'np' ? 'सुरु हुने मिति:' : 'Start Date:') ?></strong>
                                            <div><code><?= date("M d, l", strtotime($event->date_end)); ?></code></div>
                                        </div>
                                        <div class="col-md-12" style="text-align:left;">
                                            <p><?php echo word_limiter($lang == 'np' ? strip_tags($event->description_np) : $event->description, 40); ?> </p>
                                            <div class=" col-md-12" style="text-align:right;">
                                                <a href="<?= front_url('event/view/'.url_encrypt($event->id)) ?>"
                                                   class="btn btn-default btn-xs">read more</a>
                                            </div>
                                        </div>

                                        <!--                                    <h4>Magnify the Leader in You!</h4>-->
                                        <!--                                    <span>12.11.2016</span>-->
                                        <!--                                    <p>-->
                                        <? //= word_limiter('Complete development of our personality and higher potentials lies right within us. The only need is to identify our inner centers that', 9) ?><!--</p>-->
                                        <!--                                    <a href="#" class="btn">View More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="c-update__schedule">
                                <?php
                                if ( $events ) {
                                    foreach ($events as $event): ?>
                                        <div class="schedule__box">
                                            <div class="row">

                                                <div class="col-sm-10">
                                                    <div class="schedule__box--info">

                                                        <h4><?= ($lang == 'np' ? $event->title_np : $event->title) ?></h4>
                                                        <p><?php echo word_limiter($lang == 'np' ? strip_tags($event->description_np) : $event->description, 2); ?> </p>
                                                        <a href="<?= front_url('event/view/'.url_encrypt($event->id)) ?>"><i
                                                                    class="fa fa-chevron-circle-right"
                                                                    aria-hidden="true"></i></a>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="schedule__box--date">
                                                        <span><?= date("M d", strtotime($event->date_start)); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach;
                                } else {
                                    ?>

                                    <div class="schedule__box">
                                        <div class="row">

                                            <div class="col-sm-10">

                                                <div class="schedule__box--info">

                                                    <h4>No upcoming events</h4>
                                                </div>


                                            </div>

                                            <div class="col-sm-2">
                                                <div class="schedule__box--date">
                                                    <span> sorry</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- /Updates -->

    <!-- Program -->
    <section class="c-program">
        <div class="container">
            <div class="">
                <div class="c-heading text-center">
                    <h2><?= ($lang === 'np' ? 'हाम्रो कार्यक्रम' : 'Our Program') ?></h2>
                    <span> <?= ($lang === 'np' ? '
हामी प्रायः यी गतिविधिहरू गरिरहेका छौं' : 'We are doing frequently these activities') ?></span>
                </div>
                <div class="c-content text-center">
                    <div class="row">

                        <?php foreach ($programmes as $programme) { ?>
                            <div class="col-sm-3">
                                <div class="c-program__box">
                                    <div class="c-program--logo">
                                        <img height="85px" width="85px" class="img-thumbnail"
                                             src="<?= base_url(Utility::getUploadDir('program').$programme->image) ?>"
                                             alt="<?= ($lang == 'np' ? $programme->title_np : $programme->title) ?>">
                                    </div>
                                    <h3><?= ($lang == 'np' ? $programme->title_np : $programme->title) ?></h3>
                                    <p><?= word_limiter($lang == 'np' ? $programme->description_np : $programme->description, 20) ?></p>
                                    <a href="<?= front_url(sprintf("program/%s", $programme->slug)) ?>"
                                       class="btn btn--readmore"><?= $lang === 'np' ? 'अझ बढी
 ' : 'Read more' ?></a>
                                </div>
                            </div>
                        <?php }
                        if ( !$programmes ) {
                            ?>

                            No Programmes for now

                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Program -->

    <!-- Feature Video -->
    <section class="c-view">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="video-container">
                        <iframe width="470" height="315" src="https://www.youtube-nocookie.com/embed/8TwBaYn6J4I?rel=0"
                                frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-sm-4 video-desc">
                    <h3>Intensive meditation and spiritual practice programs</h3>
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants will be able to attain the state of
                        spiritual growth that can otherwise be attained only through years of efforts.</p>
                    <a href="#" class="btn btn--readmore">View More</a>
                </div>
                <div class="col-sm-3">
                    <strong><h2><?= ($lang === 'np' ? 'भनाइहरु' : 'Quotations') ?></h2></strong>
                    <hr>
                    <div class="">
                        <div class="list-group">
                            <div class="marquee toggleMarquee" style="height: 300px">
                                <?php
                                $allQuotations = [];
                                $allQuotations = $this->quotations->getAll(true);
                                foreach ($allQuotations as $quotation) {
                                    if ( $quotation->language == $lang ) { ?>
                                        <p><q><?= $quotation->quotation ?></q></p>
                                        <br>
                                    <?php }
                                } ?>
                            </div>
                            <?php
                            if ( !$allQuotations ) {
                                ?>


                                No Quotations

                                <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Feature Video -->

    <!-- Subscribe and Download -->
    <?php theme('partials', 'subscribe', $viewData) ?>
    <!-- Subscribe and Download -->

</main>

<!-- Footer -->
<footer class="l-footer">
    <?php theme('partials', 'footer', $viewData) ?>
</footer>
<!-- Footer -->

<!--<div class="l-question">-->
<!--    <a href="#">Question Answer <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>-->
<!--</div>-->

<div class="top pull-right">
    <a href="#"><img src="<?= assets('images/top.png', 'front') ?>" alt="to top"></a>
</div>

<!--
// SCRIPTS
-->
<?php theme('partials/scripts', 'scripts', $viewData) ?>

</body>