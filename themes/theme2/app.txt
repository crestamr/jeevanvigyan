.c-update{
  background: #666666;
  color:#fff;

}
.c-update .lf_science{
  background:#fff;
  color:#000;
  margin-bottom:10px;
  margin-right:2.5%;
  margin-left:5%;
  border-color: #000;
  border-width:2px;
}
.c-update .mh_rc{
  background:#fff;
  color:#000;
  margin-right:3%;
  margin-left:5%;
  margin-bottom:10px;
  border-color: #000;
  border-width:2px;
}
.c-update .lf_science h2,span{
  margin-left:5%;
}
.c-update .mh_rc h2,span{
  margin-left:5%;
}
.c-update .lf_science .lf_scienceheader{
   background:#666666;
   color:#fff;
   border-width:5px;
   border-color: #000;
}
.c-update .mh_rc .mh_rcheader{
   background:#666666;
   color:#fff;
   border-width:5px;
   border-color: #000;
}
