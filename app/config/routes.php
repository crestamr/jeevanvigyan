<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
if (PHP_SAPI !== 'cli') {

    $route['default_controller'] = 'front/home';

    $route[$this->config->item('admin_url') . '/login']['get'] = 'auth/login';
    $route[$this->config->item('admin_url') . '/login']['post'] = 'auth/do_login';
    $route[$this->config->item('admin_url') . '/reset-password']['get'] = 'auth/login/forget-password';
    $route[$this->config->item('admin_url') . '/reset-password']['post'] = 'auth/do_reset_password';
    $route[$this->config->item('admin_url') . '/logout'] = 'auth/logout';

    $route[$this->config->item('admin_url')] = 'dashboard';
    $route[$this->config->item('admin_url') . '/gallery'] = 'gallery/album';
    $route[$this->config->item('admin_url') . '/(.+)'] = '$1';

    $route['api/v1/(.+)'] = 'api/$1';

    $route['news/index/(.+)'] = 'front/news/index/$1';
    $route['news/index'] = 'front/news/index';
    $route['blog/index/(.+)'] = 'front/blog/index/$1';
    $route['blog/index'] = 'front/blog/index';
    $route['program/index/(.+)'] = 'front/program/index/$1';
    $route['program/index'] = 'front/program/index';
    $route['meditation/index/(.+)'] = 'front/meditation/index/$1';
    $route['meditation/index'] = 'front/meditation/index';
    $route['event/index/(.+)'] = 'front/event/index/$1';
    $route['event/index'] = 'front/event/index';
    $route['images/index/(.+)'] = 'front/images/index/$1';
    $route['images/index'] = 'front/images/index';
    $route['images/album/(.+)/index/(.+)'] = 'front/images/lists/$1/index/$2';
    $route['images/album/(.+)/index'] = 'front/images/lists/$1/index';

    $route['detail/(.+)'] = 'front/detail/index/$1';
    $route['pages/(.+)'] = 'front/pages/pageBySlug/$1';
    $route['program/(.+)'] = 'front/program/detail/$1';
    $route['news/(.+)'] = 'front/news/detail/$1';
    $route['blog/(.+)'] = 'front/blog/detail/$1';
    $route['videos/album/(:any)'] = 'front/videos/lists/$1';
    $route['videos/player/(:any)/(:any)'] = 'front/videos/detail/$1/$2';
    $route['images/album/(:any)'] = 'front/images/lists/$1';
    $route['audios/album/(:any)'] = 'front/audios/lists/$1';
    $route['(.+)'] = 'front/$1';


    $route['404_override'] = 'dashboard/error404';
    $route['translate_uri_dashes'] = FALSE;

} else {
    $route['matches'] = 'matches';
}
