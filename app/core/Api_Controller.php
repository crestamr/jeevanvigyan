<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/19/17
 * Time: 2:46 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

require APPPATH . 'libraries/REST/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Api_Controller extends REST_Controller
{
    public $language;

    function __construct($config = 'rest')
    {
        parent::__construct($config);

        $this->load->library('authentication');
        $this->authentication->setToCustomer();

        // disable develbar
        $this->load->config('develbar', true);
        $this->config->config['develbar']['enable_develbar'] = 0;

        // load api_model
        $this->load->model('apiModel', 'api');

        $this->language = $this->lang();

        $this->benchmark->mark('api_controller_start');
    }

    public function error($e)
    {
        $code = $e->getCode();
        $code = empty($code) ? self::HTTP_BAD_REQUEST : $code;

        $this->response([
            'status'    => 'error',
            'message'   => strip_tags($e->getMessage()),
            'data'      => null
        ], $code);
        $this->output->_display();
        exit();
    }

    public function auth($post=false)
    {
        try {
            $token = ($post == true)?$this->post('token'):$this->input->get_post('token');
            if (empty($token)) {
                throw new Exception('Unauthorized Access', self::HTTP_UNAUTHORIZED);
            }

            $userId = $this->api->checkUserToken($token);
            if ($userId === false) {
                throw new Exception('Authorization Failed', self::HTTP_UNAUTHORIZED);
            }
            return $userId;
        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function lang($post=false)
    {
        try {
            $lang = ($post == true) ? $this->post('lang') : $this->input->get_post('lang');
            $lang = empty($lang) ? 'en' : $lang;
            if (!in_array($lang, ['en', 'np'])) {
                throw new Exception('Invalid Language Format', self::HTTP_BAD_REQUEST);
            }

            return $lang;
        } catch (Exception $e) {
            $this->error($e);
        }
    }
}