<?php

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
* 
*/
class MY_Controller extends MX_Controller
{
    
    public function __construct()
    { 
        parent::__construct();

        $db_time_set    = $this->config->item('db_time_set');
        $time_zone      = $this->config->item('default_timezone');
        $this->db->query($db_time_set);
        date_default_timezone_set($time_zone);

        // Setup the theme
        // get the active theme settings
        $active_template_admin = ADMIN_THEME;
        $active_template_front = FRONT_THEME;
        defined('ADMIN_TMPL') OR define('ADMIN_TMPL', $active_template_admin);
        defined('FRONT_TMPL') OR define('FRONT_TMPL', $active_template_front);

        foreach (Modules::$locations as $location => $offset) {
            $dh = opendir($location);
            while ($file = readdir($dh)) {
                $path = $location.$file;
                if ($file != '.' && $file != '..' && is_dir($path)) {
                    $module = $file;
                    if (file_exists($path.'/setup.php')) {
                        Modules::load_file('setup.php', $path.'/');
                    }
                }
            }
        }

        $this->benchmark->mark('my_controller_end');
    }
}