<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 2:00 AM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('users/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New User">
                <i class="fa fa-user-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#active-tab" id="active-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Active
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#inactive-tab" role="tab" id="inactive-tab-btn" data-toggle="tab" aria-expanded="false">
                        Inactive
                    </a>
                </li>
            </ul>

            <div id="userListTab" class="tab-content">
                <!-- Active Users -->
                <div role="tabpanel" class="tab-pane fade active in" id="active-tab" aria-labelledby="active-tab-btn">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped data-table">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Full Name</th>
                                    <th>username</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Info</th>
                                    <th>User Groups</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($users as $user): if($user->status != 1) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$user->fullname?></td>
                                    <td><?=$user->username?></td>
                                    <td><?=$user->email?></td>
                                    <td><?=$user->phone?></td>
                                    <td><?=($user->designation. ' (' . $user->company . ')')?></td>
                                    <td><?=$user->groupname?></td>
                                    <td nowrap="">
                                        <a href="<?=admin_url('users/edit/'.url_encrypt($user->id))?>" class="btn btn-xs btn-success tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                            <span class="fa fa-edit"></span>
                                        </a>

                                        <?php if ($this->authentication->isUserDeletable($user->id)): ?>
                                            <a href="<?=admin_url('users/editaccount/'.url_encrypt($user->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit account">
                                                <span class="fa fa-user"></span>
                                            </a>

                                            <a href="<?=admin_url('users/editpassword/'.url_encrypt($user->id))?>" class="btn btn-xs btn-warning tooltip-warning loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="change password">
                                                <span class="fa fa-lock"></span>
                                            </a>

                                            <a href="<?=admin_url('users/editgroup/'.url_encrypt($user->id))?>" class="btn btn-xs btn-primary tooltip-primary loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="change group">
                                                <span class="fa fa-users"></span>
                                            </a>

                                            <a href="<?=admin_url('users/deactivate/'.url_encrypt($user->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, deactivate this user!" data-msg="Are you sure?" title="de-activate">
                                                <span class="fa fa-remove"></span>
                                            </a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Active Users -->

                <!-- In-active Users -->
                <div role="tabpanel" class="tab-pane fade" id="inactive-tab" aria-labelledby="inactive-tab-btn">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped <?=($cnt == count($users)+1 ? '' : 'data-table')?>">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Full Name</th>
                                <th>username</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Info</th>
                                <th>User Groups</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($users as $user): if($user->status == 1) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$user->fullname?></td>
                                    <td><?=$user->username?></td>
                                    <td><?=$user->email?></td>
                                    <td><?=$user->phone?></td>
                                    <td><?=($user->designation. ' (' . $user->company . ')')?></td>
                                    <td><?=$user->groupname?></td>
                                    <td nowrap="">
                                        <a href="<?=admin_url('users/deactivate/'.url_encrypt($user->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, re-activate this user!" data-msg="Are you sure?" title="re-activate">
                                            <span class="fa fa-refresh"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /In-active Users -->
            </div>
        </div>

    </div>
</div>

