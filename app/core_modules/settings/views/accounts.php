<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/16/17
 * Time: 9:00 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'account_form',
            'id'    => 'account_form'
        ];
        $action = admin_url('settings/accounts');
        echo form_open($action, $attributes);
        ?>

        <div class="col-sm-6">

            <div class="form-group">
                <label class="col-sm-4 control-label" for="username">Email:</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" id="email" value="<?=set_value('email', getUser()->email)?>" placeholder="Email" readonly>
                    <?=form_error('email', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="username">Username:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="username" id="username" value="<?=set_value('username', getUser()->username)?>" placeholder="Username" readonly>
                    <?=form_error('username', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="old_password">Old Password:</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" name="old_password" id="old_password" value="" placeholder="Old Password" required>
                    <?=form_error('old_password', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="password">New Password:</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" name="password" id="password" value="" placeholder="New Password" required>
                    <?=form_error('password', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="confirm_password">Confirm Password:</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" value="" placeholder="Confirm Password" required>
                    <?=form_error('confirm_password', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <div class="col-sm-10 col-sm-offset-4">
                    <input type="submit" class="btn btn-primary" value="Update" id="submit_btn">
                </div>
            </div>

        </div>

        <?=form_close()?>

    </div>
</div>

