<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/16/17
 * Time: 9:27 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'profile_form',
            'id'    => 'profile_form'
        ];
        $action = admin_url('settings/profile');
        echo form_open_multipart($action, $attributes);
        ?>
        <div class="col-sm-12">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="name_prefix">Full Name:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="name_prefix" id="name_prefix" value="<?=set_value('name_prefix', getUser()->name_prefix)?>" placeholder="Prefix">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="first_name" id="first_name" value="<?=set_value('first_name', getUser()->first_name)?>" placeholder="First Name">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="middle_name" id="middle_name" value="<?=set_value('middle_name', getUser()->middle_name)?>" placeholder="Middle Name">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name" id="last_name" value="<?=set_value('last_name', getUser()->last_name)?>" placeholder="Last Name">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="name_suffix" id="name_suffix" value="<?=set_value('name_suffix', getUser()->name_suffix)?>" placeholder="Suffix">
                        </div>
                    </div>
                    <?=form_error('name_prefix', '<span class="form-error">', '</span>')?>
                    <?=form_error('first_name', '<span class="form-error">', '</span>')?>
                    <?=form_error('middle_name', '<span class="form-error">', '</span>')?>
                    <?=form_error('last_name', '<span class="form-error">', '</span>')?>
                    <?=form_error('name_suffix', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="gender">Gender:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="">
                                <label>
                                    <input type="radio" class="flat" value="male" name="gender" id="gender_male" <?=set_radio('gender', '0', (getUser()->gender == 'male' ? TRUE : FALSE))?> required> <i></i> Male
                                </label>
                                <label>
                                    <input type="radio" class="flat" value="female" name="gender" id="gender_female" <?=set_radio('gender', '1', (getUser()->gender == 'female' ? TRUE : FALSE))?>> <i></i> Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php echo form_error('gender', '<span class="form-error">', '</span>') ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="phone">Contact #:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="phone" id="phone" value="<?=set_value('phone', getUser()->phone)?>" placeholder="Contact #">
                        </div>
                    </div>
                    <?=form_error('phone', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="company">Organization:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="company" id="company" value="<?=set_value('company', getUser()->company)?>" placeholder="Organization">
                        </div>
                    </div>
                    <?=form_error('company', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="designation">Designation:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="designation" id="designation" value="<?=set_value('designation', getUser()->designation)?>" placeholder="Designation">
                        </div>
                    </div>
                    <?=form_error('designation', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="profile_pic">Profile Picture:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-4">
                            <span class="help-text small col-sm-12">(JPG,PNG,GIF format only) - Maximum: 2MB [255px * 255px]</span>
                            <input type="file" class="form-control" name="profile_pic" id="profile_pic">
                        </div>
                        <div class="col-sm-8">
                            <img src="<?=userProfilePics(getUser())?>" alt="<?=getUser()->username?>" class="profile_pics" width="100" height="100" />
                            <input type="hidden" name="prevPPName" id="prevPPName" value="<?=getUser()->profile_pic?>">
                        </div>
                    </div>
                    <?=form_error('profile_pic', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="submit" class="btn btn-primary" value="Update" id="submit_btn">
                </div>
            </div>

        </div>

        <?php echo form_close() ?>

    </div>
</div>
