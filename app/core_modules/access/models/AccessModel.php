<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 5:21 PM
 */
class AccessModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAccessControl()
    {
        $this->load->library('database');
        $qry_res    = $this->db->query("CALL proc_access_control()");
        $res        = $qry_res->result();
        $this->database->next_result(); // Dump the extra resultset.
        $qry_res->free_result(); // Does what it says :)
        return $res;
    }

    public function getAccessByGroupId($id) {
        $sql = "SELECT
					DISTINCT M.id
					, (CASE WHEN ISNULL(C.amodule_id) THEN 0 ELSE 1 END) AS has_access
					, M.name
					, M.description
				FROM {$this->db->dbprefix(TBL_ACCESS_MODULES)} AS M
				LEFT JOIN {$this->db->dbprefix(TBL_ACCESS_CONTROL)} AS C
					ON
						M.id = C.amodule_id
						AND C.ugroup_id = '{$id}'
				ORDER BY M.name
				";
        return $this->db->query($sql)->result();
    }
}