<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 5:21 PM
 */
class AccessController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('access_module')
        ];
        $this->breadcrumb->append('Access Control', admin_url('access'));

        $this->load->model('accessModel', 'access');
    }

    public function manage()
    {
        $this->breadcrumb->append('Manage', admin_url('access/manage'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Access Control : Manage',
            'subTitle'  => 'List of all access control for user groups. '
        ];

        self::$viewData['accesses'] = $this->access->getAccessControl();
        $this->load->admintheme('manage', self::$viewData);
    }

    public function control()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Group Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $updateData = [
                    'name'			=> $this->input->post('name'),
                    'description'	=> $this->input->post('description')
                ];

                $result = $this->authentication->updateGroup($id, $updateData);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['group'] = $this->authentication->groupById($id);
            self::$viewData['modules'] = $this->access->getAccessByGroupId($id);
            $form = $this->load->view('control', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

}