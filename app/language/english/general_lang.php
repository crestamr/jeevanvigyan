<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/10/17
 * Time: 4:36 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Name:  General Lang - English
 *
 * Description:  English language file for Genera Messages and View*
 */

// About Software
$lang['software_name']      = 'JeevanVigyan';
$lang['software_acronym']   = 'JeevanVigyan';
$lang['software_developer'] = 'Puncoz Nepal <info@puncoz.com>';

// Authentication Related Message
$lang['invalid_login'] = 'Invalid Login!!!';
$lang['success_login'] = 'Login Success! Redirecting...';
$lang['user_expired']  = 'User Authentication Expired!!!';
$lang['must_login']    = 'You must log in to continue.';
$lang['admin_only']    = 'You must be an administrator to view this page.';
$lang['welcome_msg']   = 'Welcome to the %s!!!';

// System Modules
// :Core
$lang['login_module']     = 'Login';
$lang['dashboard_module'] = 'Dashboard';
$lang['settings_module']  = 'Settings';
$lang['users_module']     = 'User Management';
$lang['access_module']    = 'Access Control';
// :General
$lang['slider_module']     = 'Slider';
$lang['pages_module']      = 'Pages Management';
$lang['message_module']    = 'Messages';
$lang['meditation_module'] = 'Meditation ';
$lang['gallery_module']    = 'Gallery ';
$lang['menu_module']       = 'Menu Management';

// :Front
$lang['home_front_module']     = 'Home';
$lang['search_front_module']   = 'Search';
$lang['category_front_module'] = 'Category';
$lang['detail_front_module']   = 'Publication';
// :Front - Fixed Pages
$lang['page_front_module_contact'] = 'Contact';

/* Messages */
// General
$lang['direct_scripts_access'] = 'No direct script access allowed!!!';
$lang['invalid_request']       = 'Invalid Request!!!';
$lang['invalid_parameters']    = 'Invalid Parameters!!!';
$lang['service_unavail']       = 'Service unavailable at the moment, please try again later!!!';

$lang['success_added']   = 'Added Successfully!!!';
$lang['error_added']     = 'Error occurred while adding!!!';
$lang['success_updated'] = 'Updated Successfully!!!';
$lang['nothing_updated'] = 'Nothing Updated!!!';
$lang['error_updated']   = 'Error occurred while updating!!!';
$lang['success_delete']  = 'Deleted Successfully!!!';
$lang['error_delete']    = 'Error occurred while deleting!!!';

$lang['password_changed'] = 'Password changed successfully!!!';

// Front
$lang['front:contact_success'] = 'Thank You! Your message has been sent successfully. We will contact you shortly.';
$lang['front:contact_error']   = 'Error sending message, please try again later.';