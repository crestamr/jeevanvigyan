<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FeedbackModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function add($data)
	{
		$this->db->insert(TBL_FEEDBACK, $data);
		return $this->db->affected_rows() > 0 ? true : false;
	}
	
	public function getAll($activeOnly = false, $limit = null, $offset = 0)
	{
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
	
		return $this->db->get(TBL_FEEDBACK)->result();
	}

	public function getAllByUser($user, $activeOnly = false, $limit = null, $offset = 0)
    {
        $this->db->where('customer_id', $user);
        return $this->getAll($activeOnly. $limit, $offset);
    }
	
	public function getById($id)
	{
		$data = $this->db->where('id', $id)->get(TBL_FEEDBACK);
		return $data->num_rows() > 0 ? $data->row() : false;
	}
}