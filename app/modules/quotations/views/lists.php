<?php
/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 01/06/17
 * Time: 3:15 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('quotations/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Quotation">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Status</th>
                    <th>Quotation</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($quotations as $quotation): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('quotations/changestatus/'.url_encrypt($quotation->id))?>" class="tooltip-<?=($quotation->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($quotation->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($quotation->status == 1 ? 'success' : 'danger')?>"><?=($quotation->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=$quotation->quotation?></td>
                        <td nowrap="">
                            
                                <a href="<?=admin_url('quotations/edit/'.url_encrypt($quotation->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                    <span class="fa fa-edit"></span>
                                </a>

                                <a href="<?=admin_url('quotations/delete/'.url_encrypt($quotation->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                                </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($quotations) == 0): ?>
                    <tr>
                        <td colspan="6">
                            No quotations has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

