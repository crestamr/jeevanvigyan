<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 01/06/17
 * Time: 3:15 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class QuotationsController extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
	
		self::$viewData['pageInfo'] = (object)[
		'title' => $this->lang->line('quotations_module')
		];
		$this->breadcrumb->append('Quotations', admin_url('quotations/lists'));
	
		// load quotations model
		$this->load->model('QuotationsModel', 'quotations');
	}

	public function index()
    {
	   $this->lists();
	}
	
	//Listing all the quotations
	
	public function lists()
    {	
		$this->breadcrumb->append('Lists', admin_url('lists'));
		
		self::$viewData['pageDetail'] = (object)[
    		'title'     => 'Quotations',
    		'subTitle'  => 'List of Quotations'
		];
		self::$viewData['quotations'] = $this->quotations->getAll();
		$this->load->admintheme('lists', self::$viewData);
	}
	
	//Adding new quotation
	
	public function add()
    {	
		if (!$this->input->is_ajax_request()) {
			show_error($this->lang->line('direct_scripts_access'));
		}
		
		$response=[];
		if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {
			
			//form validation
			
			$this->form_validation->set_rules('quotation', 'Quotation', 'required');
            $this->form_validation->set_rules('language', 'Language', 'required');

				
			if($this->form_validation->run() === false) {
				// validation failed
				$response = Utility::getFormError();
			}else{
                // validation success

                // Update Information in database
                $insertData = [
                    'quotation' 		        => $this->input->post('quotation'),
                    'language' 	        => $this->input->post('language'),
                    'status'			=> 1
                ];

                if($this->quotations->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('quotation', 'Quotation', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $updateData = [
                    'quotation' 		        => $this->input->post('quotation'),
                    'language' 	        => $this->input->post('language'),
                    'status'			=> 1
                ];

                if($this->quotations->update($id, $updateData)) {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->quotations->getById($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->quotations->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('quotations/lists', 'refresh');
    }

    public function undodelete()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->quotations->undodelete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('quotations/lists', 'refresh');
    }
	
	public function changestatus()
	{
		$id = Utility::getIdFromUri(4);
	
		if ($id === false) {
			$this->session->set_notification('error', $this->lang->line('invalid_request'));
		} else {
			if( ! $this->quotations->changestatus($id) ) {
				$this->session->set_notification('error', $this->lang->line('nothing_updated'));
			} else {
				$this->session->set_notification('success', $this->lang->line('success_updated'));
			}
		}
		admin_redirect('quotations/lists', 'refresh');
	}
    
}