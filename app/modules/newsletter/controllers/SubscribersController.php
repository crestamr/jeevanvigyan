<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 17/06/2017
 * Time: 10:29
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SubscribersController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('newsletter_module')
        ];
        $this->breadcrumb->append('NewsLetter', admin_url('newsletter/subscribers'));

        //load contact model
        $this->load->model('newsletter/subscribersModel','subscribers');
    }

    public function index()
    {
        $this->breadcrumb->append('Subscribers', admin_url('newsletter/subscribers'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Subscribers',
            'subTitle'  => 'List of subscribers of newsletter'
        ];

        self::$viewData['subscribers'] = $this->subscribers->getAll();
        $this->load->admintheme('subscribers/lists',self::$viewData);
    }

    public function changestatus()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->subscribers->changestatus($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('newsletter/subscribers', 'refresh');
    }
}