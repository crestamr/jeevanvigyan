<?php
/**
 * Created by PhpStorm.
 * User: prabesh
 * Date: 1/18/17
 * Time: 3:23 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Asked by</th>
                        <th>Questions</th>
                        <th>Answers</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($qnas as $qna): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('qna/status/'.url_encrypt($qna->id))?>" class="tooltip-<?=($qna->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($qna->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($qna->status == 1 ? 'success' : 'danger')?>"><?=($qna->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=date('Y-m-d', strtotime($qna->created_at))?></td>
                        <td><?=$qna->asked_by ?></td>
                        <td><?=$qna->question?></td>
                        <td><?=$qna->answer?></td>
                       
                        <td nowrap="">
                            <a href="<?=admin_url('qna/edit/'.url_encrypt($qna->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="answer">
                                <span class="fa fa-pencil fa-fw"></span>
                            </a>

                       </td>
                    </tr>
                <?php endforeach; ?>
                
                </tbody>
            </table>
        </div>

    </div>
</div>
