<?php
(defined('BASEPATH')) || exit('No direct script access allowed');

class HomeController extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
	
		self::$viewData['pageInfo'] = (object)[
		'title' => $this->lang->line('slider_module'),
		'subTitle'=>'Home'
		];
		$this->breadcrumb->append('Pages', admin_url('pages/'));
	
		// load slider model
		$this->load->model('HomeModel', 'home');
	}
	
	public function index(){
	
		
		$this->form_validation->set_rules('title', 'Title', 'prep_for_form|strip_image_tags|encode_php_tags|required');
		$this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
		$this->form_validation->set_rules('content', 'Content', 'prep_for_form|strip_image_tags|encode_php_tags|required');
		$this->form_validation->set_rules('content_np', 'Content (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
		
		
		if($this->form_validation->run() === false) {
			// load form
			
			$this->breadcrumb->append('Add', admin_url('pages/home/add'));
		
			self::$viewData['pageDetail'] = (object)[
			'title'     => 'Page Management: Add Home',
			'subTitle'  => 'Add home.'
					];
		
			self::$viewData['home'] = $this->home->get();
			$this->load->admintheme('home/form', self::$viewData);
		} else {
			// validation success
		
				// Update Information in database
				$insertData = [
					'title' 	    => $this->input->post('title'),
					'title_np' 	    => $this->input->post('title_np'),
					'content' 	    => $this->input->post('content'),
					'content_np' 	=> $this->input->post('content_np'),
				];
		
				if($this->home->save($insertData)) {
					$this->session->set_alert('success', $this->lang->line('success_updated'));
				} else {
					$this->session->set_alert('error', $this->lang->line('nothing_updated'));
				}
			
			admin_redirect('pages/home');
		}
	}	
}
