<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 3:03 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class ArticleController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('pages_module')
        ];
        $this->breadcrumb->append('Pages Management', admin_url('pages'));
        $this->breadcrumb->append('Articles', admin_url('pages/article'));

        // load model
        $this->load->model('articleModel', 'article');
        $this->load->model('categoryModel', 'category');
    }

    public function index()
    {
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Page Management: Articles',
            'subTitle'  => 'List of all articles.'
        ];

        self::$viewData['articles'] = $this->article->getAll();

        $this->load->admintheme('article/lists', self::$viewData);
    }

    public function add()
    {
        $this->form_validation->set_rules('cat_id', 'Category', 'required|numeric');
        $this->form_validation->set_rules('pub_date', 'Date of publish', 'required');
        $this->form_validation->set_rules('title', 'Title', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('content', 'Content', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('content_np', 'Content (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

        if($this->form_validation->run() === false) {
            // load form
            $this->breadcrumb->append('Add', admin_url('pages/article/add'));

            self::$viewData['pageDetail'] = (object)[
                'title'     => 'Page Management: Add Article',
                'subTitle'  => 'Add new article.'
            ];

            self::$viewData['categories'] = $this->category->getAll();
            $this->load->admintheme('article/form', self::$viewData);
        } else {
            // validation success

            // Upload image first if available
            $image = null;
            if( ! empty($_FILES['image']['name']) ) {
                $image = Utility::upload_image('pages', 'image');
            }

            if($image !== false) {
                // Update Information in database
                $insertData = [
                    'cat_id' 	    => $this->input->post('cat_id'),
                    'pub_date' 	    => $this->input->post('pub_date'),
                    'title' 	    => $this->input->post('title'),
                    'title_np' 	    => $this->input->post('title_np'),
                    'content' 	    => $this->input->post('content'),
                    'content_np' 	=> $this->input->post('content_np'),
                    'image' 		=> $image
                ];

                if($this->article->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }
            }

            admin_redirect('pages/article');
        }
    }

    public function edit()
    {
        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
            admin_redirect('pages/article');
        }

        $this->form_validation->set_rules('cat_id', 'Category', 'required|numeric');
        $this->form_validation->set_rules('pub_date', 'Date of publish', 'required');
        $this->form_validation->set_rules('title', 'Title', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('content', 'Content', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('content_np', 'Content (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

        if($this->form_validation->run() === false) {
            // load form
            $this->breadcrumb->append('Edit', admin_url('pages/article/edit'));

            self::$viewData['pageDetail'] = (object)[
                'title'     => 'Page Management: Update Article',
                'subTitle'  => 'Update article.'
            ];

            self::$viewData['categories'] = $this->category->getAll();
            self::$viewData['edit'] = $this->article->getById($id);
            $this->load->admintheme('article/form', self::$viewData);
        } else {
            // validation success

            // Upload image first if available
            $prevImageName = $this->input->post('prevImageName');
            if( ! empty($_FILES['image']['name']) ) {
                $image = Utility::upload_image('pages', 'image');
                // delete previous image
                $this->article->deleteImage($prevImageName);
            } else {
                $image = $prevImageName;
            }

            if($image !== false) {
                // Update Information in database
                $udpateData = [
                    'cat_id' 	    => $this->input->post('cat_id'),
                    'pub_date' 	    => $this->input->post('pub_date'),
                    'title' 	    => $this->input->post('title'),
                    'title_np' 	    => $this->input->post('title_np'),
                    'content' 	    => $this->input->post('content'),
                    'content_np' 	=> $this->input->post('content_np'),
                    'image' 		=> empty($image) ? null : $image
                ];

                if($this->article->update($id, $udpateData)) {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_updated'));
                }
            }

            admin_redirect('pages/article');
        }
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->article->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('pages/article', 'refresh');
    }

    public function status()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->article->status($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('pages/article', 'refresh');
    }
}