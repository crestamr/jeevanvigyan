<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/25/17
 * Time: 4:01 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'article_form',
            'id'    => 'article_form'
        ];
        $action = admin_url('pages/article/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open_multipart($action, $attributes);
        ?>
        <div class="col-sm-12">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="cat_id">Category: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <select class="form-control chosen-select" name="cat_id" id="cat_id" data-placeholder="SELECT CATEGORY" required>
                                <option value=""></option>
                                <?php foreach ($categories as $category): ?>
                                    <option value="<?=$category->id?>" <?=(isset($edit) && $edit->cat_id == $category->id ? 'selected' : '')?>><?=$category->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?=form_error('cat_id', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="pub_date">Pub. Date: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="pub_date" id="pub_date datetimepicker" value="<?=set_value('pub_date', (isset($edit) ? date('Y-m-d h-i-s' ,strtotime($edit->pub_date)) : date('Y-m-d  h-i-s')))?>" placeholder="Date of Publication">
                        </div>
                    </div>
                    <?=form_error('pub_date', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="title">Title: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" id="title" value="<?=set_value('title', (isset($edit) ? $edit->title : ''))?>" placeholder="Title">
                        </div>
                    </div>
                    <?=form_error('title', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="title_np">Title (in Nepali): <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title_np" id="title_np" value="<?=set_value('title_np', (isset($edit) ? $edit->title_np : ''))?>" placeholder="Title (in Nepali)">
                        </div>
                    </div>
                    <?=form_error('title_np', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">Content: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="content" id="content" placeholder="Content"><?=set_value('content', (isset($edit) ? $edit->content : ''))?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content_np">Content (in Nepali): <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="content_np" id="content_np" placeholder="Content (in Nepali)"><?=set_value('content_np', (isset($edit) ? $edit->content_np : ''))?></textarea>
                        </div>
                    </div>
                    <?=form_error('content_np', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="image">Image:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-<?=(isset($edit) ? '4' : '6')?>">
                            <span class="help-text small col-sm-12">(JPG,PNG,GIF format only) - Maximum: 2MB</span>
                            <input type="file" class="form-control" name="image" id="image" accept="image/*">
                        </div>
                        <?php if(isset($edit)): ?>
                        <div class="col-sm-8">
                            <img src="<?=base_url(Utility::getUploadDir('pages').$edit->image)?>" alt="" height="100" />
                            <input type="hidden" name="prevImageName" id="prevImageName" value="<?=$edit->image?>">
                        </div>
                        <?php endif ?>
                    </div>
                    <?=form_error('image', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="submit" class="btn btn-primary" value="<?=(isset($edit) ? 'Update' : 'Save')?>" id="submit_btn">
                    <a href="<?=admin_url('pages/article')?>" class="btn btn-default">Cancel</a>
                </div>
            </div>

        </div>

        <?php echo form_close() ?>

    </div>
</div>

