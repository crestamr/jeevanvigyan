<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 4:04 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('pages/article/add')?>" class="btn btn-success btn-xs tooltip-success" data-toggle="tooltip" title="Add New Article">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($articles as $article): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('pages/article/status/'.url_encrypt($article->id))?>" class="tooltip-<?=($article->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($article->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($article->status == 1 ? 'success' : 'danger')?>"><?=($article->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=date('Y-m-d', strtotime($article->pub_date))?></td>
                        <td><?=$this->category->getById($article->cat_id)->name?></td>
                        <td><?=$article->title?></td>
                        <td>
                            <?php if (!empty($article->image)): ?>
                                <img src="<?=base_url(Utility::getUploadDir('pages').$article->image)?>" class="img-responsive thumbnail slider-thumbnail">
                            <?php endif ?>
                        </td>
                        <td nowrap="">
                            <a href="<?=admin_url('pages/article/edit/'.url_encrypt($article->id))?>" class="btn btn-xs btn-info tooltip-info" data-toggle="tooltip" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('pages/article/delete/'.url_encrypt($article->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? You can not undo this once confirm." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($articles) == 0): ?>
                    <tr>
                        <td colspan="7">
                            No articles has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>