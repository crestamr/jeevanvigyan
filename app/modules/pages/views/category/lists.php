<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 3:17 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('pages/category/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Category">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($categories as $category): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <span class="label label-<?=($category->status == 1 ? 'success' : 'danger')?>"><?=($category->status == 1 ? 'Active' : 'Deleted')?></span>
                        </td>
                        <td><?=$category->name?></td>
                        <td><?=$category->description?></td>
                        <td nowrap="">
                            <?php if ($category->status == 0): ?>
                                <a href="<?=admin_url('pages/category/undodelete/'.url_encrypt($category->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, un-delete it!" data-msg="Are you sure?" title="undo delete">
                                    <span class="fa fa-refresh"></span>
                                </a>
                            <?php else: ?>
                                <a href="<?=admin_url('pages/category/edit/'.url_encrypt($category->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                    <span class="fa fa-edit"></span>
                                </a>

                                <?php if ($category->editable == 1): ?>
                                <a href="<?=admin_url('pages/category/delete/'.url_encrypt($category->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure?" title="delete">
                                    <span class="fa fa-remove"></span>
                                </a>
                                <?php endif ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($categories) == 0): ?>
                    <tr>
                        <td colspan="6">
                            No categories has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

