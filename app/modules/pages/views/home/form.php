<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/25/17
 * Time: 4:01 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageInfo->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
		<?php echo alert() ?>
        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'home_form',
            'id'    => 'home_form'
        ];
        $action = admin_url('pages/home');
        echo form_open_multipart($action, $attributes);
        ?>
        <div class="col-sm-12">
        	<div class="form-group">
                <label class="col-sm-2 control-label" for="title">Title:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" id="title" value="<?=set_value('title', ($home !== false ? $home->title : ''))?>" placeholder="Title">
                        </div>
                    </div>
                    <?=form_error('title', '<span class="form-error">', '</span>')?>
                </div>
            </div>
        
            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">Content:</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="content" id="content" placeholder="Content"><?=set_value('content', ($home !== false ? $home->content : ''))?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>
			
			<hr/>
			
			<div class="form-group">
                <label class="col-sm-2 control-label" for="title_np">Title (in Nepali):</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title_np" id="title_np" value="<?=set_value('title_np', ($home !== false ? $home->title_np : ''))?>" placeholder="Title (in Nepali)">
                        </div>
                    </div>
                    <?=form_error('title_np', '<span class="form-error">', '</span>')?>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="content_np">Content (in Nepali):</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="content_np" id="content_np" placeholder="Content (in Nepali)"><?=set_value('content_np', ($home !== false ? $home->content_np : ''))?></textarea>
                        </div>
                    </div>
                    <?=form_error('content_np', '<span class="form-error">', '</span>')?>
                </div>
            </div>
        </div>
		
		<div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="submit" class="btn btn-primary" value="Update" id="submit_btn">
                </div>
            </div>
        <?php echo form_close() ?>

    </div>
</div>

