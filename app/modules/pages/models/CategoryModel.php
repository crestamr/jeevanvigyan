<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 3:15 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class CategoryModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $data['slug']   = Utility::generateSlug($data['name'], TBL_PAGES_CATEGORY, 'slug');
        $this->db->insert(TBL_PAGES_CATEGORY, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $date['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id)->update(TBL_PAGES_CATEGORY, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_PAGES_CATEGORY)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->update(TBL_PAGES_CATEGORY, ['status' => 0, 'updated_at' => date("Y-m-d H:i:s")]);
        return $this->db->affected_rows() > 0;
    }

    public function undodelete($id)
    {
        $this->db->where('id', $id)->update(TBL_PAGES_CATEGORY, ['status' => 1, 'updated_at' => date("Y-m-d H:i:s")]);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('name, created_at')->get(TBL_PAGES_CATEGORY)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_PAGES_CATEGORY);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}