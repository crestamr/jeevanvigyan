<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/24/17
 * Time: 10:52 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        $this->db->where('cat_id', CAT_NEWS);
        return $this->db->order_by('pub_date, title', 'desc')->get(TBL_PAGES_ARTICLE)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_PAGES_ARTICLE);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getBySlug($slug)
    {
        $data = $this->db->where('slug', $slug)->get(TBL_PAGES_ARTICLE);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}