<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/11/17
 * Time: 3:25 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class HomeController extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('home_front_module'),
            'keywords' => $this->lang->line('home_front_module'),
            'description' => $this->lang->line('home_front_module'),
        ];

        $this->load->model('slider/sliderModel', 'slider');
        self::$viewData['sliders'] = $this->slider->getAll(true);

        $this->load->model('events/eventsModel', 'events');
        self::$viewData['events'] = $this->events->getAll(true, 3);

        self::$viewData['event'] = $this->events->getLatest();

        $this->load->model('program/programModel', 'program');
        self::$viewData['programmes'] = $this->program->getAll(true, 4);

        $this->load->model('quotations/quotationsModel', 'quotations');
        self::$viewData['quotations'] = $this->quotations->getAll(true);

        $this->load->model('about/aboutModel', 'about');
        self::$viewData['about'] = $this->about->getFirstRow();


        $this->load->fronttheme(null, self::$viewData, 'home.index');
    }

    public function lang()
    {
        $lang = $this->uri->segment(3);
        if($lang === 'en') {
            $this->session->set_userdata('lang', 'en');
        } else if($lang === 'np') {
            $this->session->set_userdata('lang', 'np');
        }

        $gotoPage = $this->input->get('page');
        redirect(front_url($gotoPage));
    }
}