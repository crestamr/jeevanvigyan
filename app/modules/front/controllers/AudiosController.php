<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 03/08/2017
 * Time: 16:52
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class AudiosController
 */
class AudiosController extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('gallery/albumModel', 'album');
        $this->load->model('gallery/filesModel', 'audio');

    }

    public function index()
    {
        $this->album();
    }

    public function album()
    {
        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('audio_front_module'),
            'keywords' => $this->lang->line('audio_front_module'),
            'description' => $this->lang->line('audio_front_module'),
        ];

        self::$viewData['albums'] = $this->album->getAll(true, null, 0, 3);

        $this->load->fronttheme('audios/albumlist', self::$viewData);
    }

    public function lists()
    {
        $slug = $this->uri->segment(3);
        if (empty($slug) || !$album = $this->album->getBySlug($slug)) {
            show_404();
            exit();
        }
        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('audio_gallery_front_module'),
            'keywords'    => $this->lang->line('audio_gallery_front_module'),
            'description' => $this->lang->line('audio_gallery_front_module'),
        ];


        self::$viewData['album'] = $album;
        $albumId = $album->id;

        self::$viewData['audios'] = $this->audio->getAll($albumId, true);

        $this->load->fronttheme('audios/audiolist', self::$viewData);
    }
}