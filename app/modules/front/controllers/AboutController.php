<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 5/25/17
 * Time: 10:10 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class ContactController
 */
class AboutController extends Front_Controller
{
    /**
     * AboutController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('about/aboutModel','about');
    }

    public function index()
    {
        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('about_front_module'),
            'keywords'    => $this->lang->line('about_front_module'),
            'description' => $this->lang->line('about_front_module'),
        ];

        self::$viewData['about'] = $this->about->getFirstRow();
        $this->load->fronttheme('about', self::$viewData);
    }


}
