<?php

/**
 * Created by PhpStorm.
 * User: prabesh
 * Date: 5/20/17
 * Time: 11:35 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class MeditationController
 */
class EventController extends Front_Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('events/EventsModel', 'events');
        // $this->load->model('meditation/MeditationDetailModel', 'meditationDetail');
    }

    public function index($offset = 0)
    {
        // Pagination
        $config = pagination(base_url('event/index'), count($this->events->getAll(true)), 10);
        if($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['eventPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('events_front_module'),
            'keywords'    => $this->lang->line('events_front_module'),
            'description' => $this->lang->line('events_front_module'),
        ];

        self::$viewData['events'] = $this->events->getAll(true, 0, $offset);
        $this->load->fronttheme('event/lists', self::$viewData);

    }

    public function view()
    {
        $id = Utility::getIdFromUri(3);
        if ( $id === false ) {
            redirect(front_url('events'));
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('_front_module'),
            'keywords'    => $this->lang->line('meditation_details_front_module'),
            'description' => $this->lang->line('meditation_details_front_module'),
        ];

        self::$viewData['event'] = $this->events->getById($id);
        $this->load->fronttheme('event/view', self::$viewData);
    }

}
