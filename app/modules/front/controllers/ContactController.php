<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 11:35 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class ContactController
 */
class ContactController extends Front_Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact/ContactModel','contact');
    }

    public function index()
    {
        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('contact_front_module'),
            'keywords'    => $this->lang->line('contact_front_module'),
            'description' => $this->lang->line('contact_front_module'),
        ];

        $this->load->fronttheme('contact', self::$viewData);
    }

    public function add()
    {
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            //form validation

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('subject', 'Suject', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required');


            if ($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $insertData = [
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message'),
                    'is_read' => 0
                ];

                $this->contact->add($insertData);
                redirect('contact');
            }
        }
    }
}
