<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 03/08/2017
 * Time: 14:40
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class ImageController
 */
class ImagesController extends Front_Controller
{
    /**
     * ImagesController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('gallery/albumModel', 'album');

        $this->load->model('gallery/filesModel', 'image');
    }

    /**
     * Image Album page
     */

    public function index($offset = 0){
        $this->album($offset);
    }

    /**
     *
     */
    public function album($offset)
    {
        // Pagination
        $config = pagination(base_url('images/index'), count($this->album->getAll(true, null, 0, 1)), 4);
        if($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['albumPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('image_front_module'),
            'keywords' => $this->lang->line('image_front_module'),
            'description' => $this->lang->line('image_front_module'),
        ];

        self::$viewData['albums'] = $this->album->getAll(true, 4, $offset, 1);

        $this->load->fronttheme('images/albumlist', self::$viewData);
    }

    /**
     * Audio List
     */
    public function lists($offset = 0)
    {
        $slug = $this->uri->segment(3);
        if (empty($slug) || !$album = $this->album->getBySlug($slug)) {
            show_404();
            exit();
        }

        self::$viewData['album'] = $album;
        $albumId = $album->id;

        // Pagination
//        $config = pagination(base_url('images/album/'.$slug.'/index'), count($this->image->getAll($albumId, true)), 20, '5');
//        if($config != NULL) {
//            $this->pagination->initialize($config);
//            self::$viewData['imagePagination'] = $this->pagination->create_links();
//        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('image_gallery_front_module'),
            'keywords'    => $this->lang->line('image_gallery_front_module'),
            'description' => $this->lang->line('image_gallery_front_module'),
        ];

        self::$viewData['images'] = $this->image->getAll($albumId, true);

        $this->load->fronttheme('images/imagelist', self::$viewData);
    }

}