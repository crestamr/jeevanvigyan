<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 9:21 AM
 */
class NewsController extends Front_Controller
{

    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pages/newsModel', 'news');

    }

    /**
     *
     */
    public function index($offset = 0)
    {
        // Pagination
        $config = pagination(base_url('news/index'), count($this->news->getAll(true)));
        if($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['newsPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('news_front_module'),
            'keywords'    => $this->lang->line('news_front_module'),
            'description' => $this->lang->line('news_front_module'),
        ];

        self::$viewData['news'] = $this->news->getAll(true, 5, $offset);

        $this->load->fronttheme('news/lists', self::$viewData);
    }

    /**
     *
     */
    public function detail()
    {
        $slug = $this->uri->segment(2);
        if (empty($slug) || !$news = $this->news->getBySlug($slug)) {
            show_404();
            exit();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('news_front_module'),
            'keywords'    => $this->lang->line('news_front_module'),
            'description' => $this->lang->line('news_front_module'),
        ];

        self::$viewData['news'] = $news;

        $this->load->fronttheme('news/detail', self::$viewData);
    }
}
