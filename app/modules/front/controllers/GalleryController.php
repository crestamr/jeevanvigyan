<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/16/17
 * Time: 10:45 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class GalleryController
 */
class GalleryController extends Front_Controller
{

    /**
     * GalleryController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('gallery/albumModel', 'album');
    }

    /**
     * Gallery Page
     */
    public function index()
    {
        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('gallery_front_module'),
            'keywords'    => $this->lang->line('gallery_front_module'),
            'description' => $this->lang->line('gallery_front_module'),
        ];

        self::$viewData['albums']    = $this->album->getAll(true);
        self::$viewData['albumType'] = ['1' => 'Image', '2' => 'Video', '3' => 'Audio'];

        $this->load->fronttheme('gallery/index', self::$viewData);
    }

    public function image()
    {
        $id = Utility::getIdFromUri(3);
        if ( $id === false ) {
            redirect(front_url('gallery'));
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('image_gallery_front_module'),
            'keywords'    => $this->lang->line('image_gallery_front_module'),
            'description' => $this->lang->line('image_gallery_front_module'),
        ];

        $this->load->model('gallery/filesModel', 'images');
        self::$viewData['album'] = $this->album->getById($id);
        self::$viewData['images'] = $this->images->getAll($id);

        $this->load->fronttheme('gallery/images', self::$viewData);
    }

    public function video()
    {
        $id = Utility::getIdFromUri(3);
        if ( $id === false ) {
            redirect(front_url('gallery'));
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('video_gallery_front_module'),
            'keywords'    => $this->lang->line('video_gallery_front_module'),
            'description' => $this->lang->line('video_gallery_front_module'),
        ];

        $this->load->model('gallery/filesModel', 'videos');
        self::$viewData['album'] = $this->album->getById($id);
        self::$viewData['videos'] = $this->videos->getAll($id);

        $this->load->fronttheme('gallery/videos', self::$viewData);
    }

    public function audio()
    {
        $id = Utility::getIdFromUri(3);
        if ( $id === false ) {
            redirect(front_url('gallery'));
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('audio_gallery_front_module'),
            'keywords'    => $this->lang->line('audio_gallery_front_module'),
            'description' => $this->lang->line('audio_gallery_front_module'),
        ];

        $this->load->model('gallery/filesModel', 'audios');
        self::$viewData['album'] = $this->album->getById($id);
        self::$viewData['audios'] = $this->audios->getAll($id);

        $this->load->fronttheme('gallery/audios', self::$viewData);
    }
}
