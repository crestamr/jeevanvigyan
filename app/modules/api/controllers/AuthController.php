<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/19/17
 * Time: 2:54 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends Api_Controller
{
    private $db_table;

    public function __construct()
    {
        parent::__construct();

        $this->db_table = $this->authentication->getAuthTable();
    }

    public function register_post()
    {
        try {

            $this->form_validation->set_rules('first_name', 'First name', 'required');
            $this->form_validation->set_rules('last_name', 'Last name', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required|is_unique['.$this->db_table['users'].'.username]');
            $this->form_validation->set_rules('email', 'Email', 'required|is_unique['.$this->db_table['users'].'.email]|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('min_password_length', 'ion_auth') .']');
            $this->form_validation->set_rules('dob', 'Date of Birth');
            $this->form_validation->set_rules('phone', 'Phone number');
            $this->form_validation->set_rules('gender', 'Gender');
            $this->form_validation->set_rules('addressline', 'Address Line');
            $this->form_validation->set_rules('city', 'City');
            $this->form_validation->set_rules('country', 'Country');
            $this->form_validation->set_rules('occupation', 'Occupation');
            $this->form_validation->set_rules('organization', 'Organization');
            $this->form_validation->set_rules('education', 'Education Qualification');
            $this->form_validation->set_rules('institution', 'Institution');

            if ($this->form_validation->run() === false) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $username = $this->post('username');
            $password = $this->post('password');
            $email    = $this->post('email');
            $userData = [
                'first_name'   => $this->post('first_name'),
                'last_name'    => $this->post('last_name'),
                'dob'          => $this->post('dob'),
                'phone'        => $this->post('phone'),
                'gender'       => $this->post('gender'),
                'addressline'  => $this->post('addressline'),
                'city'         => $this->post('city'),
                'country'      => $this->post('country'),
                'occupation'   => $this->post('occupation'),
                'organization' => $this->post('organization'),
                'education'    => $this->post('education'),
                'institution'  => $this->post('institution'),
                'active'       => 1,
            ];

            if ($this->authentication->registerCustomer($username, $password, $email, $userData) === false) {
                throw new Exception('Registration failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response(
                [
                    'status'  => 'success',
                    'message' => 'Registration Success.',
                    'data'    => null,
                ],
                self::HTTP_OK
            );
        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function login_post()
    {
        try {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() === false) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $username = $this->post('username');
            $password = $this->post('password');

            // login user
            $login = $this->authentication->login($username, $password, false);
            if (!$login) {
                throw new Exception($this->authentication->errors(), self::HTTP_UNAUTHORIZED);
            }

            $user = $this->api->getUserProfile();
            if ($user === false) {
                throw new Exception('User profile not found!', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $token = $this->api->getUserToken($user['id']);
            if (!$token) {
                throw new Exception('Internal Server Error', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response(
                [
                    'status'  => 'success',
                    'message' => null,
                    'data'    => [
                        'token'   => $token,
                        'profile' => $user,
                    ],
                ],
                self::HTTP_OK
            );

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function reset_post()
    {
        try {
            $this->form_validation->set_rules('username', 'Username', 'required');

            if ($this->form_validation->run() === false) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $username = $this->post('username');

            if(!$this->authentication->forgotten_password($username)) {
                throw new Exception($this->authentication->errors(), self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response(
                [
                    'status'  => 'success',
                    'message' => "Email sent to reset password.",
                    'data'    => null,
                ],
                self::HTTP_OK
            );
        } catch (Exception $exception) {
            $this->error($exception);
        }
    }
}