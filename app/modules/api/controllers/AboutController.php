<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 15/08/2017
 * Time: 13:12
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AboutController extends Api_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('about/aboutModel', 'about');
    }

    public function index_get()
    {
        try {
            $about = $this->about->getFirstRow();
            $aboutResp = [];
                $aboutResp = [
                    'id'            => $about->id,
                    'home_intro'    => ($this->language == 'np') ? $about->home_intro_np : $about->home_intro,
                    'about_intro'    => ($this->language == 'np') ? $about->about_intro_np : $about->about_intro,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['about'].$about->image),
                ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $aboutResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }
}