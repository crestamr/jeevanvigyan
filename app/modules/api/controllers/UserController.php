<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/8/17
 * Time: 8:47 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends Api_Controller
{
    protected $user;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();
    }

    public function profile_get()
    {
        try {

            $profile = $this->authentication->getUser($this->user);

            $profileResp = [
                'id'           => $profile->id,
                'username'     => $profile->username?$profile->username:"",
                'email'        => $profile->email?$profile->email:"",
                'first_name'   => $profile->first_name?$profile->first_name:"",
                'last_name'    => $profile->last_name?$profile->last_name:"",
                'dob'          => $profile->dob?$profile->dob:"",
                'phone'        => $profile->phone?$profile->phone:"",
                'gender'       => $profile->gender?$profile->gender:"",
                'addressline'  => $profile->addressline?$profile->addressline:"",
                'city'         => $profile->city?$profile->city:"",
                'country'      => $profile->country?$profile->country:"",
                'occupation'   => $profile->occupation?$profile->occupation:"",
                'organization' => $profile->organization?$profile->organization:"",
                'education'    => $profile->education?$profile->education:"",
                'institution'  => $profile->institution?$profile->institution:"",
            ];

            $this->response(
                [
                    'status'  => 'success',
                    'message' => 'User Profile',
                    'data'    => $profileResp,
                ],
                self::HTTP_OK
            );

        } catch (Exception $exception) {
            $this->error($exception);
        }
    }

    public function profile_post()
    {
        try {
            $this->form_validation->set_rules('first_name', 'First name', 'required');
            $this->form_validation->set_rules('last_name', 'Last name', 'required');
            $this->form_validation->set_rules('dob', 'Date of Birth');
            $this->form_validation->set_rules('phone', 'Phone number');
            $this->form_validation->set_rules('gender', 'Gender');
            $this->form_validation->set_rules('addressline', 'Address Line');
            $this->form_validation->set_rules('city', 'City');
            $this->form_validation->set_rules('country', 'Country');
            $this->form_validation->set_rules('occupation', 'Occupation');
            $this->form_validation->set_rules('organization', 'Organization');
            $this->form_validation->set_rules('education', 'Education Qualification');
            $this->form_validation->set_rules('institution', 'Institution');

            if ($this->form_validation->run() === false) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $userData = [
                'first_name'   => $this->post('first_name'),
                'last_name'    => $this->post('last_name'),
                'dob'          => $this->post('dob'),
                'phone'        => $this->post('phone'),
                'gender'       => $this->post('gender'),
                'addressline'  => $this->post('addressline'),
                'city'         => $this->post('city'),
                'country'      => $this->post('country'),
                'occupation'   => $this->post('occupation'),
                'organization' => $this->post('organization'),
                'education'    => $this->post('education'),
                'institution'  => $this->post('institution'),
                'active'       => 1,
            ];

            if ($this->authentication->update($this->user, $userData) === false) {
                throw new Exception('Profile update failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response(
                [
                    'status'  => 'success',
                    'message' => 'Profile update Success.',
                    'data'    => null,
                ],
                self::HTTP_OK
            );
        } catch (Exception $exception) {
            $this->error($exception);
        }
    }

    public function password_post()
    {
        try {
            $this->form_validation->set_rules('old_password', 'Old Password', 'required');
            $this->form_validation->set_rules('password', 'New Password', 'required|min_length['. $this->config->item('min_password_length', 'ion_auth') .']');

            if ($this->form_validation->run() === false) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $user = $this->authentication->getUser($this->user);
            $oldPass = $this->post('old_password');
            $newPass = $this->post('password');

            if ($this->authentication->changePassword($user->username, $oldPass, $newPass) === false) {
                throw new Exception('Password change failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response(
                [
                    'status'  => 'success',
                    'message' => 'Password changed Successfully.',
                    'data'    => null,
                ],
                self::HTTP_OK
            );
        } catch (Exception $exception) {
            $this->error($exception);
        }
    }
}
