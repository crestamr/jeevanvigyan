<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 11:56
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ProgramController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('program/programModel', 'program');

        $this->user = $this->auth();
    }

    public function index_get()
    {
        try {

            $programmes = $this->program->getAll(true);
            $programmesResp = [];
            foreach ($programmes as $key => $program) {
                $programmesResp[$key] = [
                    'id'            => $program->id,
                    'title'         => ($this->language == 'np') ? $program->title_np : $program->title,
                    'description'       => ($this->language == 'np') ? $program->description_np : $program->description,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['program'].$program->image),
                    'created_at'    => $program->created_at
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $programmesResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $program = $this->program->getById($id);
            if (!$program) {
                throw new Exception("Program not found", self::HTTP_NOT_FOUND);
            }

            $programmesResp = [
                'id'            => $program->id,
                'title'         => ($this->language == 'np') ? $program->title_np : $program->title,
                'description'       => ($this->language == 'np') ? $program->description_np : $program->description,
                'image'         => base_url($this->config->item('file_upload_dir_user')['program'].$program->image),
                'created_at'    => $program->created_at
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $programmesResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

}