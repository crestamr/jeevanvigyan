<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 12:22 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EventsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_EVENTS, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_EVENTS, $data);
        return $this->db->affected_rows() > 0;
    }

    public function changestatus($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_EVENTS)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function unpublishWhenDateExpire($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_EVENTS)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);
        $this->deleteImage($prevData->cover_pics);
        $this->db->where('id', $id)->delete(TBL_EVENTS);
        return $this->db->affected_rows() > 0;
    }

    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('events').$imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }
        return false;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('status', 'DESC');
        return $this->db->order_by('date_start','ASC')->get(TBL_EVENTS)->result();
    }


    public function getLatest()
    {
        $data = $this->db->order_by('date_start','ASC')->where('status',1)->limit(1)->get(TBL_EVENTS);
        return $data->num_rows() > 0 ? $data->row() : false;
    }


	//nepali 
	public function getAllNep($activeOnly = false, $limit = null, $offset = 0){
		
		$this->db->select('id,title,date_start,date_end,cover_pics,title_np as title,description_np as description, status');
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
		
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
		
		return $this->db->order_by('date_start, status')->get(TBL_EVENTS)->result();
	}
	//english
	public function getAllEng($activeOnly = false, $limit = null, $offset = 0){
	
		$this->db->select('id,title,date_start,date_end,cover_pics,title,description, status');
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
	
		return $this->db->order_by('date_start, status')->get(TBL_EVENTS)->result();
	}
    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_EVENTS);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function userBooked($user)
    {
        return $this->db->select(TBL_EVENTS.'.*')
                        ->from(TBL_EVENTS)
                        ->join(TBL_EVENTS_BOOK, TBL_EVENTS.'.id = '.TBL_EVENTS_BOOK.'.event')
                        ->where('user', $user)
                        ->where(TBL_EVENTS.'.status', '1')
                        ->where(TBL_EVENTS_BOOK.'.status', '1')
                        ->order_by('date_start')
                        ->get()->result();
    }

}