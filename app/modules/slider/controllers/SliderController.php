<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 3:17 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SliderController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('slider_module')
        ];
        $this->breadcrumb->append('Slider', admin_url('slider'));

        // load slider model
        $this->load->model('sliderModel', 'slider');
    }

    public function index()
    {
        $this->breadcrumb->append('List', admin_url('slider'));
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Sliders: List',
            'subTitle'  => 'List of all sliders.'
        ];

        self::$viewData['sliders'] = $this->slider->getAll();

        $this->load->admintheme('lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('order_by', 'Order', 'required|numeric');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $image = $this->upload_image();

                if($image !== false) {
                    // Update Information in database
                    $insertData = [
                        'title' 		=> $this->input->post('title'),
                        'title_np' 	    => $this->input->post('title_np'),
                        'caption' 		=> $this->input->post('caption'),
                        'caption_np' 	=> $this->input->post('caption_np'),
                        'order_by' 	    => $this->input->post('order_by'),
                        'image' 		=> $image
                    ];

                    if($this->slider->add($insertData)) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('order_by', 'Order', 'required|numeric');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $prevImageName = $this->input->post('prevImageName');
                if( ! empty($_FILES['image']['tmp_name']) ) {
                    $image = $this->upload_image();
                    // delete previous image
                    $this->slider->deleteImage($prevImageName);
                } else {
                    $image = $prevImageName;
                }

                if($image !== false) {
                    // Update Information in database
                    $updateData = [
                        'title' 		=> $this->input->post('title'),
                        'title_np' 	    => $this->input->post('title_np'),
                        'caption' 		=> $this->input->post('caption'),
                        'caption_np' 	=> $this->input->post('caption_np'),
                        'order_by' 	    => $this->input->post('order_by'),
                        'image' 		=> $image
                    ];

                    if($this->slider->update($id, $updateData)) {
                        $this->session->set_notification('success', $this->lang->line('success_updated'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->slider->getById($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    private function upload_image()
    {
        $config['upload_path'] = Utility::getUploadDir('slider');
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']	= '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('image')) {
            $this->session->set_notification('error', $this->upload->display_errors());
            return false;
        } else {
            $data = ['upload_data' => $this->upload->data()];
            return $data['upload_data']['file_name'];
        }
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->slider->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('slider', 'refresh');
    }

    public function status()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->slider->status($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('slider', 'refresh');
    }
}