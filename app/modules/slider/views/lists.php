<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 3:23 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('slider/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Slider">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Caption</th>
                        <th>Image</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($sliders as $slider): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('slider/status/'.url_encrypt($slider->id))?>" class="tooltip-<?=($slider->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($slider->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($slider->status == 1 ? 'success' : 'danger')?>"><?=($slider->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=date('Y-m-d', strtotime($slider->created_at))?></td>
                        <td><?=$slider->title?></td>
                        <td><?=$slider->caption?></td>
                        <td>
                            <img src="<?=base_url(Utility::getUploadDir('slider').$slider->image)?>" class="img-responsive thumbnail slider-thumbnail">
                        </td>
                        <td nowrap="">
                            <a href="<?=admin_url('slider/edit/'.url_encrypt($slider->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('slider/delete/'.url_encrypt($slider->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($sliders) == 0): ?>
                    <tr>
                        <td colspan="7">
                            No sliders has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
