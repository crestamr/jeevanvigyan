<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 3:07 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class FilesModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $data['slug'] = Utility::generateSlug($data['title'], TBL_GALLERY_FILES, 'slug');
        $this->db->insert(TBL_GALLERY_FILES, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_GALLERY_FILES, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_GALLERY_FILES)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id, $deleteFile = true)
    {
        $prevData = $this->getById($id);

        $this->db->where('id', $id)->delete(TBL_GALLERY_FILES);
        if ($this->db->affected_rows() > 0) {
            if ($deleteFile === true) {
                $this->deleteImage($prevData->file);
            }
            return true;
        }
        return false;
    }

    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('gallery_images').$imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }

        $audioFile = Utility::getUploadDir('gallery_audio').$imageFile;
        if(file_exists($audioFile)) {
            @unlink($audioFile);
            return true;
        }

        return false;
    }

    public function undodelete($id)
    {
        $this->db->where('id', $id)->update(TBL_GALLERY_FILES, ['status' => 1, 'updated_at' => date("Y-m-d H:i:s")]);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($albumId, $activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        $this->db->where('album', $albumId);
        return $this->db->order_by('title, created_at')->get(TBL_GALLERY_FILES)->result();
    }

    public function getLatestFile($albumId, $activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        $this->db->where('album', $albumId);
        $data = $this->db->order_by('created_at','DESC')->get(TBL_GALLERY_FILES);
        return $data->num_rows() > 0 ? $data->row() : false;

    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_GALLERY_FILES);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getBySlug($slug)
    {
        $data = $this->db->where('slug', $slug)->get(TBL_GALLERY_FILES);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}