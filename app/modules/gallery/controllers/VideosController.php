<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 3:52 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class VideosController extends Admin_Controller
{
    private $albumData;

    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('gallery_module')
        ];
        $this->breadcrumb->append('Gallery', admin_url('gallery'));
        $this->breadcrumb->append('Album', admin_url('gallery/album'));
        $this->breadcrumb->append('Videos', admin_url('gallery/videos'));

        // load album detail
        $this->load->model('albumModel', 'album');
        $albumId = Utility::getIdFromUri(5);
        $this->albumData = self::$viewData['album'] = $this->album->getById($albumId);
        if ($albumId === false || $this->albumData === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
            admin_redirect('gallery');
        }

        // load model
        $this->load->model('filesModel', 'videos');
    }

    public function index()
    {
        $this->lists();
    }

    public function lists()
    {
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Gallery: Videos Album "'.$this->albumData->name.'"',
            'subTitle'  => 'List of all videos of album.'
        ];

        self::$viewData['videos'] = $this->videos->getAll($this->albumData->id);

        $this->load->admintheme('videos/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('file', 'Video Link', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success
                $file = $this->input->post('file');
                // Update Information in database
                $insertData = [
                    'album'             => $this->albumData->id,
                    'title' 		    => $this->input->post('title'),
                    'title_np' 	        => $this->input->post('title_np'),
                    'caption' 		    => $this->input->post('caption'),
                    'caption_np' 	    => $this->input->post('caption_np'),
                    'file' 		        => $file,
                    'thumbnail'         => 'https://i.ytimg.com/vi/'.$file.'/hqdefault.jpg'
                ];

                if($this->videos->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('videos/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(6);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('file', 'Video Link', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $updateData = [
                    'album'             => $this->albumData->id,
                    'title' 		    => $this->input->post('title'),
                    'title_np' 	        => $this->input->post('title_np'),
                    'caption' 		    => $this->input->post('caption'),
                    'caption_np' 	    => $this->input->post('caption_np'),
                    'file' 		        => $this->input->post('file')
                ];

                if($this->videos->update($id, $updateData)) {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->videos->getById($id);
            $form = $this->load->view('videos/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(6);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->videos->delete($id, false) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('gallery/videos/lists/'.url_encrypt($this->albumData->id), 'refresh');
    }
}