<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/2/17
 * Time: 12:43 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class AudioController extends Admin_Controller
{
    private $albumData;

    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('gallery_module')
        ];
        $this->breadcrumb->append('Gallery', admin_url('gallery'));
        $this->breadcrumb->append('Album', admin_url('gallery/album'));
        $this->breadcrumb->append('Audios', admin_url('gallery/audio'));

        // load album detail
        $this->load->model('albumModel', 'album');
        $albumId = Utility::getIdFromUri(5);
        $this->albumData = self::$viewData['album'] = $this->album->getById($albumId);
        if ($albumId === false || $this->albumData === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
            admin_redirect('gallery');
        }

        // load model
        $this->load->model('filesModel', 'audio');
    }

    public function index()
    {
        $this->lists();
    }

    public function lists()
    {
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Gallery: Audio Album "'.$this->albumData->name.'"',
            'subTitle'  => 'List of all audios of album.'
        ];

        self::$viewData['audios'] = $this->audio->getAll($this->albumData->id);

        $this->load->admintheme('audio/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $audio = Utility::upload_image('gallery_audio', 'file', 'mp3|mp2|m4a|wav|wma');

                if($audio !== false) {
                    // Update Information in database
                    $insertData = [
                        'album'             => $this->albumData->id,
                        'title' 		    => $this->input->post('title'),
                        'title_np' 	        => $this->input->post('title_np'),
                        'caption' 		    => $this->input->post('caption'),
                        'caption_np' 	    => $this->input->post('caption_np'),
                        'file' 		        => $audio
                    ];

                    if($this->audio->add($insertData)) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('audio/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function bulkuploads()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            // Upload audio
            
            $audio = $this->upload_image();
            
            if($audio !== false) {
                // Update Information in database
                $insertData = [
                    'album'             => $this->albumData->id,
                    'file' 		        => $audio['file_name'],
                    'title'             => $audio['raw_name'],
                    'title_np'             => $audio['raw_name']
                ];

                if($this->audio->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            } else {
                $response = [
                    'status' 	=> 'error',
                    'data'		=> NULL,
                    'message'	=> 'Upload Failed!'
                ];
            }

        } else {
            // load form
            self::$viewData['albumId'] = $this->albumData->id;
            $form = $this->load->view('audio/bulkuploads', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(6);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('caption', 'Caption', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('caption_np', 'Caption (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $updateData = [
                    'album'             => $this->albumData->id,
                    'title' 		    => $this->input->post('title'),
                    'title_np' 	        => $this->input->post('title_np'),
                    'caption' 		    => $this->input->post('caption'),
                    'caption_np' 	    => $this->input->post('caption_np'),
                ];

                if($this->audio->update($id, $updateData)) {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->audio->getById($id);
            $form = $this->load->view('audio/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(6);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->audio->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('gallery/audio/lists/'.url_encrypt($this->albumData->id), 'refresh');
    }
    
    private function upload_image()
    {
        $config['upload_path'] = Utility::getUploadDir('gallery_audio');
        $config['allowed_types'] = 'mp3|mp2|m4a|wav|wma';
        $config['max_size']	= '10000';
        $config['remove_spaces'] = FALSE;
        
        $this->load->library('upload', $config);
        
        if( ! $this->upload->do_upload('file')) {
            $this->session->set_notification('error', $this->upload->display_errors());
            return false;
        } else {
            $data = ['upload_data' => $this->upload->data()];
            return $data['upload_data'];
        }
    }
}