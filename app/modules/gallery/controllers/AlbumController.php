<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 2:53 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class AlbumController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('gallery_module')
        ];
        $this->breadcrumb->append('Gallery', admin_url('gallery/album'));

        // load model
        $this->load->model('albumModel', 'album');
    }

    public function index()
    {
        $this->breadcrumb->append('Albums', admin_url('gallery/album'));
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Gallery: Albums',
            'subTitle'  => 'List of all albums.'
        ];

        self::$viewData['albums'] = $this->album->getAll();

        $this->load->admintheme('album/album', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('type', 'Type', 'required|numeric', [
                'numeric'   => 'Invalid Album Type'
            ]);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('name_np', 'Name (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first [if available]
                $image = NULL;
                if( ! empty($_FILES['image']['name']) ) {
                    $image = Utility::upload_image('gallery_album', 'image');
                }

                // Update Information in database
                $insertData = [
                    'name' 		    => $this->input->post('name'),
                    'name_np' 	        => $this->input->post('name_np'),
                    'description' 		=> $this->input->post('description'),
                    'description_np' 	=> $this->input->post('description_np'),
                    'image' 		    => $image,
                    'type' 	            => $this->input->post('type')
                ];

                if($this->album->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('album/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('type', 'Type', 'required|numeric', [
                'numeric'   => 'Invalid Album Type'
            ]);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('name_np', 'Name (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $prevImageName = $this->input->post('prevImageName');
                if( ! empty($_FILES['image']['tmp_name']) ) {
                    $image = Utility::upload_image('gallery_album', 'image');
                    // delete previous image
                    $this->album->deleteImage($prevImageName);
                } else {
                    $image = $prevImageName;
                }

                if($image !== false) {
                    // Update Information in database
                    $updateData = [
                        'name' 		        => $this->input->post('name'),
                        'name_np' 	        => $this->input->post('name_np'),
                        'description' 		=> $this->input->post('description'),
                        'description_np' 	=> $this->input->post('description_np'),
                        'image' 		    => $image,
                        'type' 	            => $this->input->post('type')
                    ];

                    if($this->album->update($id, $updateData)) {
                        $this->session->set_notification('success', $this->lang->line('success_updated'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->album->getById($id);
            $form = $this->load->view('album/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->album->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('gallery/album', 'refresh');
    }

}