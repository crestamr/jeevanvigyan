<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/1/17
 * Time: 11:19 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('gallery/album')?>" class="btn btn-info btn-xs tooltip-info" data-toggle="tooltip" title="Back to album">
                <i class="fa fa-reply"></i>
            </a>

            <a href="<?=admin_url('gallery/images/add/'.url_encrypt($album->id))?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Upload New Image">
                <i class="fa fa-upload"></i>
            </a>

            <a href="<?=admin_url('gallery/images/bulkuploads/'.url_encrypt($album->id))?>" class="btn btn-primary btn-xs tooltip-primary loadForm" data-toggle="tooltip" data-successbtn="" data-addclass="bootbox-modal-sm" title="Upload Bulk Images">
                Bulk Upload
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="row">

            <?php foreach($images as $image): ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="<?=base_url(Utility::getUploadDir('gallery_images').$image->file)?>" alt="<?=$image->title?>" />
                            <div class="mask">
                                <p><?=$image->caption?></p>
                                <div class="tools tools-bottom">
                                    <a href="<?=admin_url('gallery/images/edit/'.url_encrypt($album->id).'/'.url_encrypt($image->id))?>" class="loadForm" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="Edit Image">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <a href="<?=admin_url('gallery/images/delete/'.url_encrypt($album->id).'/'.url_encrypt($image->id))?>" class="confirmDiag" data-confirmbtn="Yes, delete this image!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="caption">
                            <p><?=$image->title?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;
            if (count($images) == 0): ?>
                <div class="alert alert-info" role="alert">No image uploaded in this album yet.
                    <a href="<?=admin_url('gallery/images/add/'.url_encrypt($album->id))?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Upload New Image">
                        Upload
                    </a>
                </div>
            <?php endif ?>

        </div>

    </div>
</div>
