<?php
/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 06/06/17
 * Time: 3:50 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('menu/social/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Social Menu">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Status</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Icon</th>
                        <th>Link</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($socialmenus as $socialmenu): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('menu/social/changestatus/'.url_encrypt($socialmenu->id))?>" class="tooltip-<?=($socialmenu->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($socialmenu->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($socialmenu->status == 1 ? 'success' : 'danger')?>"><?=($socialmenu->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=$socialmenu->name?></td>
                        <td><?=$socialmenu->position?></td>
                        <td>
                            <img src="<?=base_url(Utility::getUploadDir('menu_social').$socialmenu->icon)?>" class="img-responsive thumbnail slider-thumbnail" >
                        </td>
                        <td><?=$socialmenu->link?></td>
                        <td nowrap="">
                            <a href="<?=admin_url('menu/social/edit/'.url_encrypt($socialmenu->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('menu/social/delete/'.url_encrypt($socialmenu->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($socialmenus) == 0): ?>
                    <tr>
                        <td colspan="7">
                            No social menu has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
