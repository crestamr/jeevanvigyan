<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 10:56
 */
?>


<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('program/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title">Title: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?=(isset($edit) ? $edit->title : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title_np">Title (in Nepali): <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="title_np" id="title_np" class="form-control required" placeholder="Title (in Nepali)" value="<?=(isset($edit) ? $edit->title_np : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description">Description:</label>
            <div class="col-sm-10">
                <textarea class="form-control fckeditor" name="description" id="description" placeholder="Description"><?=(isset($edit) ? $edit->description : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description_np">Description (in Nepali):</label>
            <div class="col-sm-10">
                <textarea class="form-control fckeditor" name="description_np" id="description_np" placeholder="Description (in Nepali)"><?=(isset($edit) ? $edit->description_np : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="video">Youtube Video Id:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="video" id="video" placeholder="For eg: https://www.youtube.com/watch?v=>>>8TwBaYn6J4I<<<&t=2s"><?=(isset($edit) ? $edit->video : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Image (JPG,PNG format only) - Maximum: 2MB [780px * 405px]: <span class="text-red">*</span></label>
            <?php if(isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <img src="<?php echo base_url(Utility::getUploadDir('program').$edit->image) ?>" alt="$edit->title" class="img-responsive" />
                    <input type="hidden" name="prevImageName"id="prevImageName" value="<?php echo $edit->image ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="image" id="image"  accept="image/*" >
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>

<script src="<?=assets('lib/fckeditor/fckeditor.js', 'admin')?>"></script>
<script type="text/javascript">
    var base = base_url;
    var oFCKeditor = new FCKeditor('description');
    oFCKeditor.BasePath = "<?=assets('lib/fckeditor', 'admin')?>/";
    oFCKeditor.Height="200px";
    oFCKeditor.Width="100%";
    oFCKeditor.ToolbarSet="Basic";
    oFCKeditor.ReplaceTextarea() ;
</script>
<script type="text/javascript">
    var base = base_url;
    var oFCKeditor = new FCKeditor('description_np');
    oFCKeditor.BasePath = "<?=assets('lib/fckeditor', 'admin')?>/";
    oFCKeditor.Height="200px";
    oFCKeditor.Width="100%";
    oFCKeditor.ToolbarSet="Basic";
    oFCKeditor.ReplaceTextarea() ;
</script>