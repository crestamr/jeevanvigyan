<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 15/08/2017
 * Time: 13:43
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class RecentModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getAll()
    {
        
        $query = $this->db->query('SELECT id, \'\' as fk_id, title, title_np, description, description_np, created_at, cover_pics as file, date_start, date_end, status, \'event\' as type  FROM `tbl_events` UNION
SELECT id, NULL as fk_id, title, title_np, description, description_np, created_at, image as file, NULL as date_start, NULL as date_end, status, \'meditation\' as type FROM `tbl_meditation` UNION
SELECT id, cat_id as fk_id, title, title_np, content, content_np, created_at, image as file, NULL as date_start, NULL as date_end, status, \'article\' as type  FROM `tbl_pages_article` UNION
SELECT id, album as fk_id, title, title_np, caption, caption_np, created_at, file as file, NULL as date_start, NULL as date_end, status, \'video\' as type FROM `tbl_gallery_files` 
WHERE status = 1
ORDER BY created_at DESC');
        
        return $query->result();
    }
}