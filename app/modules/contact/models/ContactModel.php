<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 04/06/17
 * Time: 1:19 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class ContactModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_CONTACT_US, $data);
        return $this->db->affected_rows() > 0;
    }

    public function isRead($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_CONTACT_US)} SET is_read = 1 WHERE id = {$id}");
        return $this->db->affected_rows() > 0 ;
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->delete(TBL_CONTACT_US);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($unreadOnly = false, $limit = null, $offset = 0)
    {
        if( $unreadOnly ){
            $this->db->where('is_read',0);
        }

        if(!is_null($limit)){
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('created_at','DESC')->get(TBL_CONTACT_US)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id',$id)->get(TBL_CONTACT_US);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

}