<?php
(defined('BASEPATH')) || exit('No direct script access allowed');

class MeditationController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        self::$viewData['pageInfo'] = (object) [
            'title' => $this->lang->line('meditation_module'),
        ];
        $this->breadcrumb->append('Pages Management', admin_url('pages'));
        $this->breadcrumb->append('Articles', admin_url('pages/article'));
        
        // load model
        $this->load->model('MeditationModel', 'meditation');
        $this->load->model('MeditationDetailModel', 'meditationDetail');
    }
    
    //function to list all the meditation
    public function index()
    {
        
        self::$viewData['pageDetail'] = (object) [
            'title'    => 'Meditation: List',
            'subTitle' => 'List of  meditation.',
        ];
        
        self::$viewData['meditations'] = $this->meditation->getAll();
        
        $this->load->admintheme('meditation/lists', self::$viewData);
    }
    
    //function to add meditation
    
    public function add()
    {
        
        if ( !$this->input->is_ajax_request() ) {
            show_error($this->lang->line('direct_scripts_access'));
        }
        
        if ( strtolower($this->input->server('REQUEST_METHOD')) == 'post' ) {
            
            //print_r($this->input->post('type_id'));exit;
            //form validation
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('description_np', 'Description', 'required');
            
            if ( $this->form_validation->run() === false ) {
                // validation failed
                $response = Utility::getFormError();
            }
            else {
                
                //uploading image  (first param as folder name and second post name)
                $image = Utility::upload_image("meditation", "image");
                //saving data in to database
                if ( $image !== false ) {
                    $insertData = [
                        'title'          => $this->input->post('title'),
                        'title_np'       => $this->input->post('title_np'),
                        'description'    => $this->input->post('description'),
                        'description_np' => $this->input->post('description_np'),
                        'image'          => $image,
                        'status'         => 1,
                    ];
                    
                    if ( $this->meditation->add($insertData) ) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    }
                    else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }
                $response = [
                    'status'  => 'success',
                    'data'    => NULL,
                    'message' => 'success',
                ];
                
            }
            
        }
        else {
            
            // load form
            $form     = $this->load->view('form', self::$viewData, true);
            $response = [
                'status'  => 'success',
                'data'    => null,
                'message' => $form,
            ];
        }
        
        $this->output->json($response);
        
    }
    
    //function to edit meditation
    public function edit()
    {
        
        if ( !$this->input->is_ajax_request() ) {
            show_error($this->lang->line('direct_scripts_access'));
        }
        
        $id = Utility::getIdFromUri(4);
        if ( $id === false ) {
            $this->output->json([
                'status'  => 'error',
                'data'    => NULL,
                'message' => $this->lang->line('invalid_request'),
            ]);
        }
        $response = [];
        if ( strtolower($this->input->server('REQUEST_METHOD')) == 'post' ) {
            
            
            //form validation
            
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('description_np', 'Description', 'required');
            
            if ( $this->form_validation->run() === false ) {
                // validation failed
                $response = Utility::getFormError();
            }
            else {
                
                //uploading image  (first param as folder name and second post name)
                $prevImageName = $this->input->post('prevImageName');
                if ( !empty($_FILES['image']['tmp_name']) ) {
                    $image = $image = Utility::upload_image("meditation", "image");
                    // delete previous image
                    $this->meditation->deleteImage($prevImageName);
                }
                else {
                    $image = $prevImageName;
                }
                
                // Update Information in database
                $updateData = [
                    'title'          => $this->input->post('title'),
                    'title_np'       => $this->input->post('title_np'),
                    'description'    => $this->input->post('description'),
                    'description_np' => $this->input->post('description_np'),
                    'image'          => $image,
                ];
                
                if ( $this->meditation->update($id, $updateData) ) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                }
                else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }
                
                $response = [
                    'status'  => 'success',
                    'data'    => NULL,
                    'message' => 'success',
                ];
            }
            
        }
        else {
            // load form with data
            self::$viewData['edit'] = $this->meditation->getById($id);
            $form                   = $this->load->view('form', self::$viewData, true);
            $response               = [
                'status'  => 'success',
                'data'    => null,
                'message' => $form,
            ];
        }
        $this->output->json($response);
        
    }
    
    //function to add meditation details
    public function add_meditation_details()
    {
        
        if ( !$this->input->is_ajax_request() ) {
            show_error($this->lang->line('direct_scripts_access'));
        }
        
        $meditation_id = Utility::getIdFromUri(4);
        
        if ( strtolower($this->input->server('REQUEST_METHOD')) == 'post' ) {
            $file = !empty($this->input->post('url')) ? $this->input->post('url') : Utility::upload_image('meditation', 'file', 'jpg|png|jpeg|mp3|ogg|wav|aac|wma|m4a|mp2');
            
            if ( $file !== false ) {
                $insertData = [
                    'med_id' => $this->input->post('meditation_id'),
                    'type'   => $this->input->post('type_id'),
                    'value'  => $file,
                ];
                
                if ( $this->meditationDetail->add($insertData) ) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                }
                else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }
            }
            
            $response = [
                'status'  => 'success',
                'data'    => NULL,
                'message' => 'success',
            ];
            
        }
        else {
            // load form with data
            self::$viewData['meditation_id'] = $meditation_id;
            $form                            = $this->load->view('meditation_details/form', self::$viewData, true);
            $response                        = [
                'status'  => 'success',
                'data'    => null,
                'message' => $form,
            ];
        }
        $this->output->json($response);
    }
    
    //functio to get meditation details
    public function get_meditation_details()
    {
        
        self::$viewData['pageDetail'] = (object) [
            'title'    => 'Meditation: List',
            'subTitle' => 'List of  meditation file.',
        ];
        
        
        $id                                   = Utility::getIdFromUri(4);
        self::$viewData['meditation_id']      = $id;
        self::$viewData['meditation_details'] = $this->meditationDetail->getAll($id);
        $this->load->admintheme('meditation/meditation_details/lists', self::$viewData);
    }
    //function to edit meditation details
    
    //functio to delete meditation details
    public function delete_meditation_detail()
    {
        
        $id = Utility::getIdFromUri(4);
        if ( $id === false ) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        }
        else {
            if ( !$this->meditationDetail->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            }
            else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('meditation/get_meditation_details/' . url_encrypt(Utility::getIdFromUri(5)), 'refresh');
    }
    
    //function to delete the meditation
    public function delete()
    {
        
        $id = Utility::getIdFromUri(4);
        
        if ( $id === false ) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        }
        else {
            if ( !$this->meditation->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            }
            else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('meditation/', 'refresh');
    }
    
    //function to change the status of meditation
    
    public function status()
    {
        
        $id = Utility::getIdFromUri(4);
        if ( $id == false ) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        }
        else {
            if ( !$this->meditation->status($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            }
            else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('meditation', 'refresh');
    }
}