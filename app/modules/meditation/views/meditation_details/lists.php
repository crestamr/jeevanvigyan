
<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('meditation/add_meditation_details/'.url_encrypt($meditation_id))?>"
               class="btn btn-success btn-xs tooltip-success loadForm"
               data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm"
               title="Add New Meditation">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="table-responsive">
            <table class="table table-bordered table-hover <?=!empty($meditation_details)?'data-table':''?>">
                <thead>
                    <tr>
                    	<th>S.N</th>
                        <th>Meditation</th>
                        <th>File Type</th>
                        <th>File</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($meditation_details as $meditation_detail): ?>
                    <tr> 
                        <td><?=$cnt++?></td>
                        <td><?=isset($meditation_detail->title)?$meditation_detail->title:''?></td>
                        <td><?=isset($meditation_detail->type)?$meditation_detail->type:''?></td>
                        <td>
                        	<?php if($meditation_detail->type == 'image'):?>
                        		<img src="<?=base_url(Utility::getUploadDir('meditation').$meditation_detail->value)?>" class="img-responsive thumbnail slider-thumbnail">
                        	<?php elseif ($meditation_detail->type == 'video'):?>
                                <img height="50px" width="80px" src="https://i.ytimg.com/vi/<?=$meditation_detail->value?>/hqdefault.jpg">
                            <?php  endif;?>
                        </td>
                        <td nowrap="">

                            <a href="<?=admin_url('meditation/delete_meditation_detail/'.url_encrypt($meditation_detail->id).'/'.url_encrypt($meditation_id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($meditation_details) == 0): ?>
                    <tr>
                        <td colspan="6">
                            No meditations has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
