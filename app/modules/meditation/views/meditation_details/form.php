<?php
/**
 * Created by PhpStorm.
 * User: bimesh
 * Date: 1/18/17
 * Time: 3:35 PM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role' => 'form',
            'name' => 'popup_form',
            'id' => 'popup_form'
        ];
        $action = admin_url('meditation/' . (isset($edit) ? 'edit/' . url_encrypt($edit->id) : 'add_meditation_details'));
        echo form_open($action, $attributes);
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="type_id">Type: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <!--  this is for passing meditation id  -->
                <input type="hidden" value="<?php echo $meditation_id; ?>" name="meditation_id">
                <div class="row">
                    <div class="col-sm-6">
                        <select class="form-control chosen-select type_id" name="type_id" id="type_id"
                                data-placeholder="SELECT TYPE" required>
                            <option value=""></option>
                            <option value="image">image</option>
                            <option value="audio">audio</option>
                            <option value="video">video</option>
                        </select>
                    </div>
                </div>
                <?= form_error('med_id', '<span class="form-error">', '</span>') ?>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div id="hole"></div>


        <?= form_close() ?>

    </div>
</div>

<div class="file_type" style="display:none">

    <div id="electron_images">

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">File (JPEG,PNG) - Maximum: 2MB [780px * 405px]: <span
                        class="text-red">*</span></label>
            <?php if (isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <img src="<?php echo base_url(Utility::getUploadDir('events') . $edit->cover_pics) ?>" alt=""
                         class="img-responsive"/>
                    <input type="hidden" name="prevfileName" id="prevImageName" value="<?php echo $edit->cover_pics ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="file"
                       id="image">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
    </div>

    <div id="electron_audio">

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Audio(.MP3): <span class="text-red">*</span></label>
            <?php if (isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <img src="<?php echo base_url(Utility::getUploadDir('meditation') . $edit->value) ?>" alt=""
                         class="img-responsive"/>
                    <input type="hidden" name="prevfileName" id="prevImageName" value="<?php echo $edit->cover_pics ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="file"
                       id="image">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
    </div>


    <div id="electron_video">
        <label class="col-sm-12 pull-left" for="image">Video ID: <span class="text-red">*</span></label>
        <div class="form-group has-feedback">
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="text" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="url"
                       id="image"
                       placeholder="https://www.youtube.com/watch?v= >>>>> wDlPrWqGfiQ <<<<<">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.type_id').change(function () {
            var value = $('.type_id').val();

            switch (value) {

                case 'image':
                    $('#hole div').remove();
                    $('#electron_images').children().clone().appendTo('#hole');
                    break;

                case 'audio':
                    $('#hole div').remove();
                    $('#electron_audio').children().clone().appendTo('#hole');
                    break;

                case 'video':
                    $('#hole div').remove();
                    $('#electron_video').children().clone().appendTo('#hole');
                    break;
            }
        });
    })
</script>
