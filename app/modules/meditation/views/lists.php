<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('meditation/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Meditation">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover <?=!empty($meditation_details)?'data-table':''?>">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($meditations as $meditation): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td><?=date('Y-m-d', strtotime($meditation->created_at))?></td>
                        <td><?=$meditation->title?></td>
                        <td><?=$meditation->description?></td>
                         <td>
                            <a href="<?=admin_url('meditation/status/'.url_encrypt($meditation->id))?>" class="tooltip-<?=($meditation->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($meditation->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($meditation->status == 1 ? 'success' : 'danger')?>"><?=($meditation->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td nowrap="">
                            <a href="<?=admin_url('meditation/edit/'.url_encrypt($meditation->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('meditation/delete/'.url_encrypt($meditation->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                            
                            <a href="<?=admin_url('meditation/get_meditation_details/'.url_encrypt($meditation->id))?>" class="btn btn-xs btn-info tooltip-info"  data-successbtn="Update" title="view" target="_blank">
                                <span class="fa fa-eye"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($meditations) == 0): ?>
                    <tr>
                        <td colspan="6">
                            No meditations has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
