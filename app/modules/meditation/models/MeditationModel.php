<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeditationModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	//function to add meditation
	public function add($data)
	{
		$this->db->insert(TBL_MEDITATION, $data);
		return $this->db->affected_rows() > 0;
	}
	
	//function to update meditation
	public function update($id,$data){
		$this->db->where('id', $id)->update(TBL_MEDITATION, $data);
		return $this->db->affected_rows() > 0;
	}
	// function to retrieve all the meditation 
	
	public function getAll($activeOnly = false, $limit = null, $offset = 0)
	{
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
		
		
		return $this->db->order_by('created_at', 'desc')->get(TBL_MEDITATION)->result();
	}
	
	//nepali
	public function getAllNep($activeOnly = false, $limit = null, $offset = 0){
	
		$this->db->select('id,image,title_np as title,description_np as description, status');
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
	
		return $this->db->get(TBL_MEDITATION)->result();
	}
	//english
	public function getAllEng($activeOnly = false, $limit = null, $offset = 0){
	
		$this->db->select('id,image,title,description, status');
		if ($activeOnly) {
			$this->db->where('status', 1);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
	
		return $this->db->get(TBL_MEDITATION)->result();
	}
	
	public function delete($id)
	{
		$this->db->where('id', $id)->delete(TBL_MEDITATION);
		return $this->db->affected_rows() > 0;
	}
	
	//function to get meditation by id
	public function getById($id){
		
		$data = $this->db->where('id',$id)->get(TBL_MEDITATION);
		return $data->num_rows() > 0 ? $data->row() : false;
	}
	//function to change status of meditation
	public function status($id){
		
			$this->db->query("UPDATE {$this->db->dbprefix(TBL_MEDITATION)} set status = 1 - status where id = {$id} ");
			return $this->db->affected_rows() > 0;
	}
    
    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('meditation').$imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }
        return false;
    }
}