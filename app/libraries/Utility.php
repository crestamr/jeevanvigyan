<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 1:20 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class Utility 
{
    public static function getFormError($value='')
    {
        $CI =& get_instance();

        $form_errors = array();
        foreach ($_POST as $key => $value) {
            $errMsg = form_error($key);
            if( !empty($errMsg) ) {
                $form_errors[] = [
                    'id' => $key,
                    'message' => $errMsg
                ];
            }
        }
        $form_errors[] = [
            'id'        => $CI->security->get_csrf_token_name(),
            'message'   => $CI->security->get_csrf_hash()
        ];

        return [
            'status'    => 'error',
            'data'      => $form_errors,
            'message'   => 'form_error'
        ];
    }

    public static function getIdFromUri($uriSegment, $isEncrypted = true)
    {
        $id = get_instance()->uri->segment($uriSegment);

        if (empty($id)) return false;

        if ($isEncrypted) {
            $id = url_decrypt($id);
        }
        return (empty($id) || !is_numeric($id)) ? false : $id;
    }

    public static function getUploadDir($type)
    {
        $uploadDir = config_item('file_upload_dir_user');
        return isset($uploadDir[$type]) ? $uploadDir[$type] : false;
    }
    
    public  static function upload_image($uploadDir, $image = "image",$allowed_type='jpg|png|jpeg')
    {
    	$CI =& get_instance();
    	$config['upload_path'] = Utility::getUploadDir($uploadDir);
    	$config['allowed_types'] = $allowed_type;
    	$config['max_size']	= '0';
    	$config['encrypt_name'] = true;
    	
    	$CI->load->library('upload', $config);
    	
    	if( ! $CI->upload->do_upload($image)) {
    		$CI->session->set_notification('error', $CI->upload->display_errors());
    		return false;
    	} else {
    		$data = ['upload_data' => $CI->upload->data()];
    		return $data['upload_data']['file_name'];
    	}
    }

    public static function generateSlug($string, $db_tbl, $db_col)
    {
        $slug = strtolower(url_title($string));
        $params = [
            $db_col => $slug
        ];

        $i = 0;
        $CI =& get_instance();
        while ($CI->db->where($params)->get($db_tbl)->num_rows()) {
            if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }

            $params [$db_col] = $slug;
        }

        return $slug;
    }

    public static function excerpt($text, $length)
    {
        if (strlen($text) > $length) {
            $text = substr($text, 0, $length);
            $text = substr($text,0,strrpos($text," "));
            $etc = "...";
            $text = $text.$etc;
        }
        return $text;
    }
}