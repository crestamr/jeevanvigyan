<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/10/17
 * Time: 4:54 PM
 */

/**
 * Authentication Class
 *
 * Extended class for ion_auth
 */
class Authentication
{
    protected $CI;

    /*
     * Current User Info
     */
    private $user = [];

    /*
     * Current User's Group Info
     */
    private $userGroup = [];

    /*
     * User's File Upload Dir
     */
    private $uploadDir = [];

    /*
     *  Constructor
     */
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('ion_auth');
        $this->CI->load->model('ion_auth_model');

        $this->initialize();
    }

    public function initialize()
    {
        $this->user      = $this->CI->ion_auth->user()->row();
        $this->userGroup = $this->CI->ion_auth->get_users_groups()->result();
        $this->uploadDir = config_item('file_upload_dir_user');
    }

    /*
     * Get Users' file upload directory
     */
    public function getUploadDir($type)
    {
        return isset($this->uploadDir[$type]) ? $this->uploadDir[$type] : '';
    }

    /*
     * Get Users' Information
     */
    public function getUser($id = null)
    {
        if (is_null($id)) {
            return $this->user;
        }
        $user = $this->CI->ion_auth->user($id);

        return $user->num_rows() > 0 ? $user->row() : false;
    }

    /*
     * Get Current Users' Group Information
     */
    public function getUserGroup($id = null)
    {
        if (is_null($id)) {
            return $this->userGroup;
        }

        return $this->CI->ion_auth->get_users_groups($id)->result();
    }

    /*
     * Check for currents user in groups
     */
    public function userIn($group_id)
    {
        $groupArr = [];
        foreach ($this->userGroup as $group) {
            $groupArr[] = $group->id;
        }

        return in_array($group_id, $groupArr) ? true : false;
    }

    /*
     * Check whether user is logged in or not
     */
    public function isUserLoggedIn()
    {
        return $this->CI->ion_auth->logged_in() ? true : false;
    }

    /*
     * Login User
     */
    public function login($identity, $password, $rememberme = false)
    {
        if (empty($identity) || empty($password)) {
            return false;
        }

        if ($this->CI->ion_auth->login($identity, $password, $rememberme)) {
            $this->user = $this->CI->ion_auth->user()->row();

            return true;
        }

        return false;
    }

    /*
     * Logout User
     */
    public function logout()
    {
        return $this->CI->ion_auth->logout();
    }

    /*
     * Change Password
     */
    public function changePassword($identity, $old, $new)
    {
        return $this->CI->ion_auth_model->change_password($identity, $old, $new);
    }

    /*
     * Update User
     */
    public function Update($id, $data)
    {
        return $this->CI->ion_auth->update($id, $data);
    }

    public function changeUserActiveStatus($userId)
    {
        $table = $this->CI->ion_auth_model->tables['users'];
        $this->CI->db->query("UPDATE {$this->CI->db->dbprefix($table)} SET active = 1 - active WHERE id = {$userId}");

        return $this->CI->db->affected_rows() > 0;
    }

    public function updateUserGroup($userId, $groupIds)
    {
        $this->CI->db->trans_start();

        // first remove all assigned groups
        $this->CI->db->where('user_id', $userId)->delete(TBL_AUTH_USERS_GROUPS);

        // now insert the newly assigned groups
        $insertGroup = [];
        foreach ($groupIds as $key => $groupId) {
            $insertGroup[] = [
                'user_id'  => $userId,
                'group_id' => $groupId,
            ];
        }

        $this->CI->db->insert_batch(TBL_AUTH_USERS_GROUPS, $insertGroup);

        $this->CI->db->trans_complete();

        return ($this->CI->db->trans_status() === false) ? false : true;
    }

    public function deleteProfilePicture($ppname)
    {
        $imageFile = './'.$this->getUploadDir('profile_pics').$ppname;
        if (file_exists($imageFile)) {
            @unlink($imageFile);

            return true;
        }

        return false;
    }

    public function groups($excludeAdmin = false)
    {
        if ($excludeAdmin) {
            $this->CI->db->where_not_in('id', UG_SUPERADMIN);
        }

        return $this->CI->ion_auth->groups()->result();
    }

    public function groupById($id)
    {
        return $this->CI->ion_auth->group($id)->row();
    }

    public function isGroupEditable($id)
    {
        $query = $this->CI->db->select('is_editable')->where('id', $id)->get($this->CI->db->dbprefix(TBL_AUTH_GROUPS));
        if ($query->num_rows() > 0) {
            return ($query->row()->is_editable == '1') ? true : false;
        } else {
            return false;
        }
    }

    public function isUserDeletable($userId)
    {
        $sql              = "SELECT MIN(B.is_userdeletable) AS is_userdeletable
				FROM {$this->CI->db->dbprefix(TBL_AUTH_USERS_GROUPS)} AS A
				JOIN {$this->CI->db->dbprefix(TBL_AUTH_GROUPS)} AS B ON A.`group_id` = B.`id`
				WHERE A.`user_id` = {$userId}";
        $is_userdeletable = $this->CI->db->query($sql);

        return ($is_userdeletable->num_rows() > 0) ? ($is_userdeletable->row()->is_userdeletable == 1 ? true : false) : false;
    }

    public function createGroup($data)
    {
        return $this->CI->ion_auth->create_group($data['name'], $data['description']);
    }

    public function updateGroup($id, $data)
    {
        return $this->CI->ion_auth->update_group($id, $data['name'], $data['description']);
    }

    public function deleteGroup($id)
    {
        return $this->CI->ion_auth->delete_group($id);
    }

    public function getAllUsers()
    {
        $sql = "SELECT
					U.`id`
					, U.`username`
					, U.`email`
					, U.`phone`
					, U.`company`
					, U.`designation`
					, CONCAT(COALESCE(U.`name_prefix`,' '), ' '
					       , COALESCE(U.`first_name`,' '), ' '
					       , COALESCE(U.`middle_name`,' '), ' '
					       , COALESCE(U.`last_name`,' '), ' '
					       , COALESCE(U.`name_suffix`,' '), ' '
					       ) AS `fullname`
					, GROUP_CONCAT(
						CONCAT(
							' '
							, UCASE(MID(G.`name`, 1, 1))
							, LCASE(MID(G.`name`, 2))
						)
					) AS groupname
					, U.`active` AS `status`
				FROM `{$this->CI->db->dbprefix(TBL_AUTH_USERS)}` AS U
				JOIN `{$this->CI->db->dbprefix(TBL_AUTH_USERS_GROUPS)}` AS UG
					ON U.`id` = UG.`user_id`
				JOIN `{$this->CI->db->dbprefix(TBL_AUTH_GROUPS)}` AS G
					ON UG.`group_id` = G.`id`
				GROUP BY U.`id`";

        return $this->CI->db->query($sql)->result();
    }

    public function register($username, $password, $email, $userData, $group)
    {
        return $this->CI->ion_auth->register($username, $password, $email, $userData, $group);
    }

    public function forgotten_password($identity)
    {
        return $this->CI->ion_auth->forgotten_password($identity);
    }

    /*
     * Error List
     */
    public function errors()
    {
        return $this->CI->ion_auth->errors();
    }

    /*
     * Messages
     */
    public function messages()
    {
        return $this->CI->ion_auth->messages();
    }

    public function getAuthTable()
    {
        return $this->CI->ion_auth_model->tables;
    }

    /************************** CUSTOMER ***************************/
    public function getAllCustomer($fetch_type = "both")
    {

        if ($fetch_type == "active") {
            $this->CI->db->where("active", 1);
        } else {
            if ($fetch_type == "inactive") {
                $this->CI->db->where("active", 0);
            }
        }

        return $this->CI->ion_auth->users()->result();

    }

    public function registerCustomer($identity, $password, $email, $additional_data = [])
    {
        $ip_address = $this->CI->input->ip_address();
        $salt       = $this->CI->ion_auth_model->store_salt ? $this->CI->ion_auth_model->salt() : false;
        $password = $this->CI->ion_auth_model->hash_password($password, $salt);
        $data = array_merge(
            $additional_data,
            [
                'username'   => $identity,
                'password'   => $password,
                'email'      => $email,
                'ip_address' => $ip_address,
                'created_on' => time(),
                'active'     => 1,
            ]
        );

        if ($this->CI->ion_auth_model->store_salt) {
            $data['salt'] = $salt;
        }

        $this->CI->db->insert($this->CI->ion_auth_model->tables['users'], $data);

        $id = $this->CI->db->insert_id();

        return isset($id) && !empty($id) ? true : false;
    }

    public function setToCustomer()
    {
        $this->CI->ion_auth_model->tables['users'] = TBL_AUTH_CUSTOMER;
        $this->initialize();
    }

    public function setToAdmin()
    {
        $this->CI->ion_auth_model->tables['users'] = TBL_AUTH_USERS;
        $this->initialize();
    }
}