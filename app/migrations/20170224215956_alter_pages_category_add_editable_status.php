<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Alter_pages_category_add_editable_status extends CI_Migration
{
    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_category          = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        $fields = [
            'editable' => [
                'type'          => 'int',
                'default'        => 1,
                'after'         => 'status'
            ],

        ];
        $this->dbforge->add_column($this->tbl_category, $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column($this->tbl_category, 'editable');
    }

    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            // table names
            $this->tbl_category            = TBL_PAGES_CATEGORY;
        }
    }
}
/* End of file '20170224215956_alter_pages_category_add_editable_status' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170224215956_alter_pages_category_add_editable_status.php */
