<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Add_field_image_to_about_us_table extends CI_Migration
{

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_about_us          = '';


    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

        $this->use_config();

    }
	
	public function up()
	{
	    $fields = [
            'image' => array(
                'type'=>'VARCHAR',
                'constraint'=>150,
                'null' => true
            )
        ];
        $this->dbforge->add_column($this->tbl_about_us, $fields);
    }

	public function down()
	{
        $this->dbforge->drop_column($this->tbl_about_us, [
            'image',
        ]);
    }


    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            // table names
            $this->tbl_about_us            = TBL_ABOUT_US;
        }
    }
}
/* End of file '20170813144821_add_field_image_to_about_us_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170813144821_add_field_image_to_about_us_table.php */
