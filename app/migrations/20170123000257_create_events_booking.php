<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_events_booking extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_event_book   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_event_book)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_event_book, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'user' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => '8',
                'unsigned'      => TRUE,
            ],
            'event' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'updated_at' => [
                'type'          => 'DATETIME',
                'null'          => true,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_event_booking_user_customer_id FOREIGN KEY (`user`) REFERENCES ".TBL_AUTH_CUSTOMER."(`id`)");
        $this->dbforge->add_field("CONSTRAINT fk_event_booking_event_event_id FOREIGN KEY (`event`) REFERENCES ".TBL_EVENTS."(`id`)");
        $this->dbforge->create_table($this->tbl_event_book);

        log_message('info', 'Event Booking Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_event_book)) {
            $this->dbforge->drop_table($this->tbl_event_book);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_event_book = TBL_EVENTS_BOOK;
        }
    }
}
/* End of file '20170123000257_create_events_booking' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170123000257_create_events_booking.php */
