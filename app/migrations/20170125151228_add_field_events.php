<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Add_field_events extends CI_Migration
{
    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_events          = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        $fields = [
            'title_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'after'         => 'title'
            ],
            'description_np' => [
                'type'          => 'TEXT',
                'null'          => TRUE,
                'after'         => 'description'
            ]
        ];
        $this->dbforge->add_column($this->tbl_events, $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column($this->tbl_events, [
            'title_np',
            'description_np'
        ]);
    }

    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            // table names
            $this->tbl_events            = TBL_EVENTS;
        }
    }
}
/* End of file '20170125151228_add_field_events' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170125151228_add_field_events.php */
