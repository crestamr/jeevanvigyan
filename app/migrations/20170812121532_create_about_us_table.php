<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_about_us_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_about_us   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if($this->db->table_exists($this->tbl_about_us)) {
            if($this->drop_table === false){
                return;
            }
            $this->dbforge->drop_table($this->tbl_about_us, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          =>'INT',
                'constraint'    =>'11',
                'unsigned'      =>TRUE,
                'auto_increment'=> TRUE
            ],
            'home_intro' => [
                'type'          => 'TEXT',
                'null'          =>TRUE
            ],
            'about_intro' => [
                'type'          => 'TEXT',
                'null'          =>TRUE
            ],
            'created_at  timestamp default current_timestamp'
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_about_us);

        log_message('info', 'About Us Table Created in Database.');
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl_about_us)){
            $this->dbforge->drop_table($this->tbl_about_us);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_about_us = TBL_ABOUT_US;
        }
    }

}
/* End of file '20170812121532_create_about_us_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170812121532_create_about_us_table.php */
