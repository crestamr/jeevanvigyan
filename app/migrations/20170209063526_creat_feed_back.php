<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Creat_feed_back extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_feedback   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_feedback)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_feedback, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'customer_id' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => '11',
        		'unsigned'      => TRUE,
            ],
            'message' => [
                'type'          => 'TEXT',
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '11',
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_feedback_customer_id FOREIGN KEY (`customer_id`) REFERENCES ".TBL_AUTH_CUSTOMER."(`id`)");
        $this->dbforge->create_table($this->tbl_feedback);

        log_message('info', 'Feedback Details Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_feedback)) {
            $this->dbforge->drop_table($this->tbl_feedback);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_feedback = TBL_FEEDBACK;
        }
    }
}
/* End of file '20170209063526_creat_feed_back' */
/* Location: ./C:\xampp\htdocs\jeevan-vigyan-web\application\migrations/20170209063526_creat_feed_back.php */
