<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_qna_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_qna   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_qna)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_qna, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'asked_by' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
            ],
            'question' => [
                'type'          => 'TEXT',
            ],
            'answer' => [
                'type'          => 'TEXT',
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '11',
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_qna_asked_by FOREIGN KEY (`asked_by`) REFERENCES ".TBL_AUTH_CUSTOMER."(`id`)");
        $this->dbforge->create_table($this->tbl_qna);

        log_message('info', 'Q&A Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_qna)) {
            $this->dbforge->drop_table($this->tbl_qna);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_qna = TBL_QNA;
        }
    }
}
/* End of file '20170314001902_create_qna_table' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170314001902_create_qna_table.php */
