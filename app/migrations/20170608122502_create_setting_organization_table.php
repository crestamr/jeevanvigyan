<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_setting_organization_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table name
    private $tbl_setting_org = '';

    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

		$this->use_config();
	}
	
	public function up()
	{
	    //Drop table if exists
        if($this->db->table_exists($this->tbl_setting_org)) {
            if($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_setting_org, true);
        }

        //Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => 11,
                'unsigned'      => true,
                'auto_increment'=> true
            ],
            'key' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => true,
                'unique'        => true
            ],
            'value' => [
                'type'          => 'TEXT',
                'null'          => true
            ],
            'created_by' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => 8,
                'unsigned'      => true
            ],
            'updated_by' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => 8,
                'unsigned'      => true
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->add_field("CONSTRAINT fk_tbl_setting_org_auth_users_created_by FOREIGN KEY (`created_by`) REFERENCES ".TBL_AUTH_USERS."(`id`)");
        $this->dbforge->add_field("CONSTRAINT fk_tbl_setting_org_auth_users_updated_by FOREIGN KEY (`updated_by`) REFERENCES ".TBL_AUTH_USERS."(`id`)");
        $this->dbforge->create_table($this->tbl_setting_org);

        log_message('info', 'Organization Setting Table Created in Database.');
    }
    
	public function down()
	{
        if($this->db->table_exists($this->tbl_setting_org)){
            $this->dbforge->drop_table($this->tbl_setting_org);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_setting_org = TBL_SETTING_ORG;
        }
    }

}
/* End of file '20170608122502_create_setting_organization_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170608122502_create_setting_organization_table.php */
