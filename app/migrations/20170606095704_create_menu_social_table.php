<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_menu_social_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_menu_social = '';

    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

        $this->use_config();
	}
	
	public function up()
	{
	    //Drop table if exists
        if($this->db->table_exists($this->tbl_menu_social)) {
            if($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_menu_social, true);
        }

        //Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => 11,
                'unsigned'      => true,
                'auto_increment'=> true,
            ],
            'position' => [
                'type'          => 'INT',
                'constraint'    => 11,
                'default'          => 0
            ],
            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => true
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => true
            ],
            'link' => [
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'null'          => true
            ],
            'icon' => [
                'type'          => 'VARCHAR',
                'constraint'    => 255,
                'null'          => true
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1
            ],
            'created_at  timestamp default current_timestamp',
            'updated_at' => [
                'type'          => 'DATETIME',
                'null'          => true
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl_menu_social);

        log_message('info', 'Menu Social table created in database.');
    }
    
	public function down()
	{
	    if($this->db->table_exists($this->tbl_menu_social)){
            $this->dbforge->drop_table($this->tbl_menu_social);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_menu_social = TBL_MENU_SOCIAL;
        }
    }
}
/* End of file '20170606095704_create_menu_social_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170606095704_create_menu_social_table.php */
