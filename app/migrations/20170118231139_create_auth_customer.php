<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_auth_customer extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_customer   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_customer)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_customer, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => '16'
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => '80',
            ],
            'salt' => [
                'type' => 'VARCHAR',
                'constraint' => '40'
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '100'
            ],
            'activation_code' => [
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ],
            'forgotten_password_code' => [
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ],
            'forgotten_password_time' => [
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'null' => TRUE
            ],
            'remember_code' => [
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ],
            'created_on' => [
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
            ],
            'last_login' => [
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'null' => TRUE
            ],
            'active' => [
                'type' => 'TINYINT',
                'constraint' => '1',
                'unsigned' => TRUE,
                'default' => 1
            ],
            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ],
            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ],
            'dob' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ],
            'gender' => [
                'type'          => 'ENUM',
                'constraint'    => ['male','female','other'],
                'null'          => TRUE,
            ],
            'addressline' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ],
            'city' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'country' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'occupation' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => TRUE,
            ],
            'organization' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'education' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'institution' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ],
            'profile_pic' => [
                'type'          => 'VARCHAR',
                'constraint'    => 255,
                'null'          => TRUE
            ],
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_customer);

        log_message('info', 'Customer Table Created in Database.');

        // Dumping data for table 'users'
        $data = [
            'id' => '1',
            'ip_address' => '127.0.0.1',
            'username' => 'user',
            'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
            'salt' => '',
            'email' => 'user@user.com',
            'activation_code' => '',
            'forgotten_password_code' => NULL,
            'created_on' => '1268889823',
            'last_login' => '1268889823',
            'active' => '1',
            'first_name' => 'Front',
            'last_name' => 'User'
        ];
        $this->db->insert($this->tbl_customer, $data);
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_customer)) {
            $this->dbforge->drop_table($this->tbl_customer);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_customer = TBL_AUTH_CUSTOMER;
        }
    }
}
/* End of file '20170118231139_create_auth_customer' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170118231139_create_auth_customer.php */
