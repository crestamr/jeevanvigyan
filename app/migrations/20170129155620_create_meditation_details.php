<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_meditation_details extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_meditation_details   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_meditation_details)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_meditation_details, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'med_id' => [
                'type'          => 'INT',
                'constraint'    => '11',
        		'unsigned'      => TRUE,
            ],
            'type' => [
                'type'          => 'VARCHAR',
                'constraint'          => '100',
            ],
            'value' => [
                'type'          => 'VARCHAR',
                'constraint'    => '100',
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_meditation_med_id FOREIGN KEY (`med_id`) REFERENCES ".TBL_MEDITATION."(`id`)");
        $this->dbforge->create_table($this->tbl_meditation_details);

        log_message('info', 'Meditation Details Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_meditation_details)) {
            $this->dbforge->drop_table($this->tbl_meditation_details);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_meditation_details = TBL_MEDITATION_DETAILS;
        }
    }
}
/* End of file '20170125154100_create_page_article' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170125154100_create_page_article.php */
