CREATE DEFINER=`root@localhost` PROCEDURE `proc_access_controls`() DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER BEGIN
	SET @sql_query = NULL;

	SELECT
		GROUP_CONCAT(DISTINCT
			CONCAT(
				'MAX(IF(A.amodule_id = ',
				id,
				', 1, IF(G.id = 1, 1, 0))) AS ',
				name
			)
		) INTO @sql_query
	FROM core_access_modules;

	SET @sql_query = CONCAT('SELECT G.id AS group_id
				, G.name AS groups, ', @sql_query, '
			FROM auth_groups G
			LEFT JOIN core_access_control AS A
				ON G.id = A.ugroup_id
			GROUP BY G.id');

	PREPARE stmt FROM @sql_query;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END